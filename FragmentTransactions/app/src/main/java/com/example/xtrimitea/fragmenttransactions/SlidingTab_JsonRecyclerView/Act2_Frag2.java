package com.example.xtrimitea.fragmenttransactions.SlidingTab_JsonRecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xtrimitea.fragmenttransactions.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Act2_Frag2 extends Fragment {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;
    private JSONArray jsonArray;

    private static final String TAG = Act2_Frag2.class.getSimpleName();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e(TAG,TAG+" onAttach");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            Log.d(TAG, "setUserVisibleHint in " + TAG);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG,TAG+" onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.stjrv_fragment_2, container, false);
        Log.d(TAG,TAG+" onCreateView");

        init(view);
        process();

        return view;
    }


    public void init(View view){
        //Get JSONArray declared by Act2_Frag1
        jsonArray = Singleton.getInstance().getJsonArray();

        tabLayout = view.findViewById(R.id.tabLayout);
        viewPager = view.findViewById(R.id.viewPager);
    }

    public void process(){
        //Set TabLayouts (Second TabLayout)
        try {
            for (int i=0; i<jsonArray.length(); i++){
                Singleton.getInstance().setCarBrands(jsonArray.getJSONObject(0)
                        .getJSONArray("CarBrands").getJSONObject(i).getString("CarBrand"));
                tabLayout.addTab(tabLayout.newTab().setText(Singleton.getInstance().getCarBrands()));
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        pagerAdapter = new PagerAdapter(getChildFragmentManager(), tabLayout.getTabCount(),1);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG,TAG+" onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG,TAG+" onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG,TAG+" onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG,TAG+" onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG,TAG+" onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG,TAG+" onDetach");
    }
}
