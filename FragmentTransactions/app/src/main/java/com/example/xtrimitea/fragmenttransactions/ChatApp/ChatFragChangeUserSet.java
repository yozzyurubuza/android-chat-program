package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ChatFragChangeUserSet extends Fragment {
    private static final String TAG = ChatFragChangeUserSet.class.getSimpleName();

    private EditText changeFname, changeLname, changeCnum, changeEmail, changePassCurr, changePassNew;
    private Button changeContactBut, changePassBut;
    private Switch editUserSwitch, changePassSwitch;
    private LinearLayout editSet1, editSet2;
    private Boolean switch1, switch2;

    private DatabaseReference userRef;
    private FirebaseUser user;
    private ProgressDialog progressDialog;
    private ProgressDialog progressDialogPass;

    private View view2, view3;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_frag_change_userset, container, false);
        Log.d(TAG, TAG+ " onCreateView");

        init(view);
        process(view);

        return view;
    }

    private void init(View view){
        //Switches and inflation codes
        editUserSwitch = view.findViewById(R.id.editUserSwitch);
        changePassSwitch = view.findViewById(R.id.changePassSwitch);
        switch1 = editUserSwitch.isChecked();
        switch2 = changePassSwitch.isChecked();
        editSet1 = view.findViewById(R.id.editSet1_layout);
        editSet2 = view.findViewById(R.id.editSet2_layout);

        userRef = FirebaseDatabase.getInstance().getReference().child("users");
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Changing details....");

        progressDialogPass = new ProgressDialog(getActivity());
        progressDialogPass.setMessage("Changing Password...");

    }

    private void process(final View view){

        //Inflate User Settings
        editUserSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                switch1 = b;
                if (switch1){
                    Log.e(TAG, "Inserting chat_frag_userset1..");
                    LayoutInflater inflater = LayoutInflater.from(getActivity());
                    view2 = inflater.inflate(R.layout.chat_frag_userset1, null);
                    editSet1.addView(view2);

                    //Display Details
                    displaySet1(view);
                }
                else{
                    Log.e(TAG, "Removing chat_frag_userset1..");
                    editSet1.removeView(view2);
                }

            }
        });
        //Inflate Change Password
        changePassSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                switch2 = b;
                if (switch2){
                    Log.e(TAG, "Inserting chat_frag_userset2..");
                    LayoutInflater inflater = LayoutInflater.from(getActivity());
                    view3 = inflater.inflate(R.layout.chat_frag_userset2, null);
                    editSet2.addView(view3);

                    //Display Change user and pass
                    displaySet2(view);

                }
                else{
                    Log.e(TAG, "Removing chat_frag_userset2..");
                    editSet2.removeView(view3);
                }

            }
        });




    }



    public void changeWarningDetails(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        builder.setTitle("Change Details")
                .setMessage("Are you sure you want to change your details?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        progressDialog.show();
                        delayChangeDetails();

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void delayChangeDetails(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                //Saving to singleton first...
                Singleton.getInstance().setFname(changeFname.getText().toString());
                Singleton.getInstance().setLname(changeLname.getText().toString());
                Singleton.getInstance().setContactnum(changeCnum.getText().toString());

                //Applying change to DB
                userRef.child(Singleton.getInstance().getCurrUid()).child("fname").setValue(Singleton.getInstance().getFname());
                userRef.child(Singleton.getInstance().getCurrUid()).child("lname").setValue(Singleton.getInstance().getLname());
                userRef.child(Singleton.getInstance().getCurrUid()).child("contactnum").setValue(Singleton.getInstance().getContactnum());

                //Set Text to Edit Texts
                changeFname.setText(Singleton.getInstance().getFname());
                changeLname.setText(Singleton.getInstance().getLname());
                changeCnum.setText(Singleton.getInstance().getContactnum());

                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Details Changed!", Toast.LENGTH_SHORT).show();
            }
        }, 3000);
    }

    public void displaySet1(View view){
        //Edit User Settings
        changeFname = view.findViewById(R.id.changeFname2);
        changeLname = view.findViewById(R.id.changeLname);
        changeCnum = view.findViewById(R.id.changeCnum);
        changeContactBut = view.findViewById(R.id.changeContactBut);

        changeFname.setText(Singleton.getInstance().getFname());
        changeLname.setText(Singleton.getInstance().getLname());
        changeCnum.setText(Singleton.getInstance().getContactnum());

        changeContactBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Show warning before changing
                changeWarningDetails();
            }
        });



    }

    public void displaySet2(View view){
        //Change Password
        changeEmail = view.findViewById(R.id.changeEmail);
        changePassCurr = view.findViewById(R.id.changePassCurr);
        changePassNew = view.findViewById(R.id.changePassNew);
        changePassBut = view.findViewById(R.id.changePassBut);
        changeEmail.setText(Singleton.getInstance().getCurrEmail());

        user = FirebaseAuth.getInstance().getCurrentUser();
        final String email = user.getEmail();
        Log.e(TAG, email);

        changePassBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialogPass.show();
                //Firebase codes to change pass by getting the current email and password

                AuthCredential credential = EmailAuthProvider.getCredential(email,changePassCurr.getText().toString());
                user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            user.updatePassword(changePassNew.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(!task.isSuccessful()){
                                        //Internet failure or others
                                        Snackbar snackbar_fail = Snackbar
                                                .make(getView(), "Something went wrong. Please try again later", Snackbar.LENGTH_LONG);
                                        snackbar_fail.show();
                                    }else {
                                        //Password matches and was changed successfully
                                        Snackbar snackbar_su = Snackbar
                                                .make(getView(), "Password Successfully Modified", Snackbar.LENGTH_LONG);
                                        snackbar_su.show();
                                        clearPass();
                                    }
                                }
                            });
                        }else {
                            //Wrong password
                            Snackbar snackbar_su = Snackbar
                                    .make(getView(), "Authentication Failed", Snackbar.LENGTH_LONG);
                            snackbar_su.show();
                            clearPass();
                        }

                    }

                });


            }
        });

    }

    public void clearPass(){
        progressDialogPass.hide();
        changePassCurr.setText("");
        changePassNew.setText("");
    }

    public void delayChangePass(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                //Set Text to Edit Texts
                changePassCurr.setText("");
                changePassNew.setText("");

                progressDialogPass.dismiss();
            }
        }, 3000);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        progressDialog.dismiss();
        progressDialogPass.dismiss();
        Log.d(TAG, TAG+ " onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");

    }


}
