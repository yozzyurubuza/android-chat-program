package com.example.xtrimitea.fragmenttransactions.ChatApp;

public class ChatInfoRegister {
    private String email, lname, fname, contactnum, imageurl;

    public ChatInfoRegister(String email, String lname, String fname,
                            String contactnum, String imageurl) {
        this.email = email;
        this.lname = lname;
        this.fname = fname;
        this.contactnum = contactnum;
        this.imageurl = imageurl;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getContactnum() {
        return contactnum;
    }

    public void setContactnum(String contactnum) {
        this.contactnum = contactnum;
    }
}

