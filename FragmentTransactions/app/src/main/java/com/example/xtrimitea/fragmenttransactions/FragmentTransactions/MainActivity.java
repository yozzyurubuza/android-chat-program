package com.example.xtrimitea.fragmenttransactions.FragmentTransactions;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xtrimitea.fragmenttransactions.R;

//4. Implement methods of the buttons
//6.1 Implement FragmentMangager.onBackStackChangedListener
public class MainActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {

    //By declaring this, it sets the manager associated with the activity
    FragmentManager manager;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        manager = getSupportFragmentManager();
        text = (TextView) findViewById(R.id.stackMsg);
        //Add this to display text of the backStack
        manager.addOnBackStackChangedListener(this);

        //Add this to automatically set the scroll movement of the text
        text.setMovementMethod(new ScrollingMovementMethod());

    }

    //4.2 Create object of the Fragment
    public void addA(View v){
        FragmentA frag1 = new FragmentA();
        //beginTransaction is necessary for the fragments to add,remove,replace stuff
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.group, frag1, "A");
        //6. Add addToBackStack in every function to put a fragment into backstack, which can be used later when the user presses back
        transaction.addToBackStack("addA");
        transaction.commit();

    }
    public void addB(View v){
        FragmentB frag2 = new FragmentB();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.group, frag2, "B");
        transaction.addToBackStack("addB");
        transaction.commit();

    }
    public void removeA(View v){

        FragmentA frag1 = (FragmentA) manager.findFragmentByTag("A");
        FragmentTransaction transaction = manager.beginTransaction();
        if (frag1 != null){
            transaction.remove(frag1);
            transaction.addToBackStack("removeA");
            transaction.commit();
            Toast.makeText(this,"Fragment A has been removed successfully", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this,"Fragment A not found", Toast.LENGTH_SHORT).show();
        }

    }
    public void removeB(View v){
        FragmentB frag2 = (FragmentB) manager.findFragmentByTag("B");
        FragmentTransaction transaction = manager.beginTransaction();
        if (frag2 != null){
            transaction.remove(frag2);
            transaction.addToBackStack("removeB");
            transaction.commit();
            Toast.makeText(this,"Fragment A has been removed successfully", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this,"Fragment A not found", Toast.LENGTH_SHORT).show();
        }

    }
    public void replaceAWithB(View v){
        FragmentB frag2 = new FragmentB();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.group, frag2, "B");
        transaction.addToBackStack("replaceAWithB");
        transaction.commit();

    }
    public void replaceBWithA(View v){
        FragmentA frag1 = new FragmentA();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.group, frag1, "A");
        transaction.addToBackStack("replaceBWithA");
        transaction.commit();
    }
    public void attachA(View v){
        FragmentA frag1 = (FragmentA) manager.findFragmentByTag("A");
        FragmentTransaction transaction = manager.beginTransaction();
        if (frag1 != null){
            transaction.attach(frag1);
            transaction.addToBackStack("attachA");
            transaction.commit();
        }
    }
    public void detachA(View v){
        FragmentA frag1 = (FragmentA) manager.findFragmentByTag("A");
        FragmentTransaction transaction = manager.beginTransaction();
        if (frag1 != null){
            transaction.detach(frag1);
            transaction.addToBackStack("detachA");
            transaction.commit();
        }
    }

    public void back(View v){
        manager.popBackStack();
    }

    public void popAddB(View v){
        manager.popBackStack("addB",0);
    }

    @Override
    public void onBackStackChanged() {

        text.setText(text.getText()+"\n");
        text.setText(text.getText()+"The current status of Back Stack: \n\n");
        int count = manager.getBackStackEntryCount();
        //6.2 Create for loop to display the entries of backStack
        for (int i=count-1;i>=0;i--){
            FragmentManager.BackStackEntry entry = manager.getBackStackEntryAt(i);
            text.setText(text.getText()+""+ entry.getName() + "");
            text.setText(text.getText()+"\n");
        }


    }
}
