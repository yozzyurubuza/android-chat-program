package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ChatFragCreateGC extends Fragment {
    private static final String TAG = ChatFragCreateGC.class.getSimpleName();

    private Button createGCBut;
    private ProgressDialog createGCDialog;
    private EditText gcNameBox;
    private String gcName;

    private FragmentManager manager;
    private FragmentTransaction transaction;

    private DatabaseReference groupRef;
    private Boolean foundData = false;
    private Boolean insertOnce = false;

    private int searchCounter = 0;
    private int numOfGC = 0;
    private int numOfGCMembers = 0;
    private List<String> addToGC = new ArrayList<String>();
    private List<Integer> numOfGCList = new ArrayList<Integer>();
    private String currUid;
    private int lastNum = 0, sizeCounter=0;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_frag_create_gc, container, false);
        Log.d(TAG, TAG+ " onCreateView");

        init(view);
        process(view);

        return view;
    }

    private void init(View view){
        createGCBut = view.findViewById(R.id.createGCBut);
        gcNameBox = view.findViewById(R.id.gcNameBox);

        currUid = Singleton.getInstance().getCurrUid();

        createGCDialog = new ProgressDialog(getActivity());
        createGCDialog.setMessage("Creating Group Chat....");

        groupRef = FirebaseDatabase.getInstance().getReference().child("groupchats");

    }

    private void process(View view){
        //Display friends retrieved from fragment home
        displayFriends();

        createGCBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addToGC = Singleton.getInstance().getAddToGC();
                addToGC.add(currUid);
                //Create Group Chat
                if(addToGC.size() > 2) {
                    createGCDialog.show();
                    createGC();
                    delayAdd();
                } else {
                    Log.e(TAG, "addToGC: "+addToGC.size());
                    Toast.makeText(getActivity(), "Please select up to 2 friends to add", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void createGC(){

        gcName = gcNameBox.getText().toString();
        //Parsing addToGC members

        groupRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snap: dataSnapshot.getChildren()) {
                    if (snap.getKey().equals(currUid)) {
                        Log.e(TAG, "Snap: " + snap);
                        Log.e(TAG, dataSnapshot.getChildrenCount() + "");
                        for (int x = 1; x < dataSnapshot.getChildrenCount() + 1; x++) {
                            String parseNumOfGC = "gc_" + currUid + "_" + x;
                            searchCounter += 1;
                            Log.e(TAG, "Search Counter: "+searchCounter);
                            if (snap.child(parseNumOfGC).getChildrenCount() != 0) {
                                if (parseNumOfGC.equals(snap.child(parseNumOfGC).getKey())) {
                                    Log.e(TAG, "parseNumOfGC found!");
                                    numOfGC += 1;
                                    Log.e(TAG, "numOfGC: " + numOfGC);
                                }
                            }
                            if (searchCounter == dataSnapshot.getChildrenCount()){
                                insertOnce = true;
                            }
                        }

                    } else {
                        Log.e(TAG, "Searching....");
                        sizeCounter +=1;
                        if (dataSnapshot.getChildrenCount() == sizeCounter && insertOnce == false) {
                            Log.e(TAG, "No GC found created by the user");
                            numOfGC = 0;
                            insertOnce = true;
                        }
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }



    public void displayFriends(){
        ChatFragCreateGCFlist gcFlist = new ChatFragCreateGCFlist();

        manager = Singleton.getInstance().getManager();
        transaction = manager.beginTransaction();
        transaction.add(R.id.recyclerGCLayout, gcFlist, "GCFlist");
        transaction.commit();
        Log.d(TAG, "Add Group Chat Flist");

    }

    public void insertGC(String uid, int numberGC){

        Singleton.getInstance().setDownloadGCImage(false);
        Log.e(TAG, "New GCListAdapterSize: "+Singleton.getInstance().getGcListAdapterSize());
        //if (!insertOnce) {
            //Create member list
            //for (int i = 0; i < addToGC.size(); i++) {
                Log.e(TAG, "Adding: " + uid);
                //Adding the GC info to members in db
                //Create chatmsgs
//                groupRef.child(uid).child("gc_" + currUid + "_" + numberGC)
//                        .child("chatmsg").child("placeholder").setValue("placeholder");

                //Create imageurl
                groupRef.child(uid).child("gc_" + currUid + "_" + numberGC)
                        .child("imageurl").setValue("http://lequytong.com/Content/Images/no-image-02.png");

                for (int j = 0; j < addToGC.size(); j++) {
                    //Create Member list
                    Log.e(TAG, "Adding Member " + addToGC.get(j) + " to user's db: " + uid);
                    groupRef.child(uid).child("gc_" + currUid + "_" + numberGC)
                            .child("members").child("member" + j).setValue(addToGC.get(j));

                }
            //}

            if (gcNameBox.getText().toString().startsWith(" ") || gcNameBox.getText().toString().equals("")) {
                //Create GC name
                gcNameBox.setText("New Group Chat " + numberGC);
                gcName = gcNameBox.getText().toString();

            } else {
                gcName = gcNameBox.getText().toString();
                Log.e(TAG, "Setting gcName: " + gcName);
            }

            //Creating GC name
            for (int i = 0; i < addToGC.size(); i++) {
                groupRef.child(addToGC.get(i)).child("gc_" + currUid + "_" + numberGC)
                        .child("gcname").setValue(gcName);
            }

            //insertOnce = true;
        //}
    }

    public void delayAdd(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if(insertOnce) {
                    Log.e(TAG, "Initiate insert GC method...");
                    numOfGC +=1;
                    for (int i = 0; i < addToGC.size(); i++) {
                        insertGC(addToGC.get(i), numOfGC);
                    }
                    insertOnce = false;
                    numOfGC = 0;
                }
                createGCDialog.dismiss();
                Toast.makeText(getActivity(), "Group Chat Created!", Toast.LENGTH_SHORT).show();
                manager.popBackStack();
            }
        }, 2000);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        createGCDialog.dismiss();
        Log.d(TAG, TAG+ " onStop");
        searchCounter = 0;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");

    }


}
