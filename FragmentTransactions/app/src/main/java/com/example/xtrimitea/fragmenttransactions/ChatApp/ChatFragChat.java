package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChatFragChat extends Fragment {
    private static final String TAG = ChatFragChat.class.getSimpleName();

    private TextView friendName;
    private ImageView friendDP;
    private Button backButton, sendButton;
    private EditText messageBox;
    private FragmentManager manager;
    private FragmentTransaction transaction;

    private DatabaseReference chatRef;
    private String messageToBeSent="";
    private String msgCodeUser = "null", msgCodeFriend;

    private int numberOfMsgs, senderNum=1, receiverNum=2;


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_frag_window, container,false);
        Log.d(TAG, TAG+ " onCreateView");

        init(view);
        process(view);


        return view;
    }


    private void init(View view) {
        friendName = view.findViewById(R.id.friendFullName);
        friendDP = view.findViewById(R.id.friendDP);
        sendButton = view.findViewById(R.id.sendBut);
        backButton = view.findViewById(R.id.backBut);
        messageBox = view.findViewById(R.id.messageBox);

        manager = Singleton.getInstance().getManager();
        Singleton.getInstance().setListAdapterPos(0);

        chatRef = FirebaseDatabase.getInstance().getReference().child("userfriends");


    }

    private void process(View view) {
        //Add chat messages fragment
        addFrag();

        //Back button function
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });

        //Send button function
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageToBeSent = messageBox.getText().toString();
                sendMsg();
            }
        });

        //Set name and DP of friend
        friendName.setText(Singleton.getInstance().getChatFriendFullName(
                Singleton.getInstance().getRecyclerClickPos()));

        try {
            friendDP.setImageBitmap(Singleton.getInstance().getImageListFriends(
                    Singleton.getInstance().getRecyclerClickPos()+1
            ));
            Log.e(TAG, "FriendDP retrieved from cache successfully!");
        }catch (Exception e){
            Log.e(TAG, "FriendDP not yet found in cache, downloading....");
            new Singleton.DownloadImageTask((ImageView) view.findViewById(R.id.friendDP))
                    .execute(Singleton.getInstance().getFriendListImages(
                            Singleton.getInstance().getRecyclerClickPos()));

        }


    }

    public void addFrag(){
        ChatFragMsgList msgList = new ChatFragMsgList();
        manager = Singleton.getInstance().getManager();
        transaction = manager.beginTransaction();
        transaction.add(R.id.scrollChatView, msgList, "Messages");
        transaction.commit();
        Log.d(TAG, "addFrag: in "+TAG);
    }

    public void removeFrag(){
        ChatFragMsgList msgList = new ChatFragMsgList();
        manager = Singleton.getInstance().getManager();
        transaction = manager.beginTransaction();
        if(msgList != null) {
            transaction.remove(msgList);
            transaction.commit();
            Log.d(TAG, "Executing remove frag2 in " + TAG);
        }
    }

    public void back(){
        manager.popBackStack();
    }

    public void sendMsg(){
        if (messageToBeSent.equals("") || messageToBeSent.startsWith(" ")) {
            Toast.makeText(getActivity(), "No message to be sent", Toast.LENGTH_SHORT).show();
        }else{
            sendMsgToDB();
        }

    }

    public void sendMsgToDB(){

        final Boolean foundData[] = {false};
        final Boolean setOnce[] = {false};

        //Function for sending message
        chatRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snap: dataSnapshot.getChildren()){
                    if (!foundData[0]) {
                        if (snap.child("email").getValue().equals(Singleton.getInstance().getCurrEmail())) {

                            snap = snap.child("chatmsg").child(Singleton.getInstance().getChatUid());
                            //Create a placeholder text if no chat has been made
                            if (snap.getValue() == null) {
                                Log.e(TAG, "No chat msgs yet, creating placeholder...");
                                //Create placeholder for the user
                                chatRef.child(Singleton.getInstance().getCurrUid()).child("chatmsg")
                                        .child(Singleton.getInstance().getChatUid()).child("placeholder").setValue("placeholder");
                                //Create placeholder for the chatmate
                                chatRef.child(Singleton.getInstance().getChatUid()).child("chatmsg")
                                        .child(Singleton.getInstance().getCurrUid()).child("placeholder").setValue("placeholder");

                            } else {
                                messageToBeSent = messageBox.getText().toString();
                                numberOfMsgs = (int) snap.getChildrenCount();
                                msgCodeUser = "msg" + numberOfMsgs + "_" + senderNum;
                                msgCodeFriend = "msg" + numberOfMsgs + "_" + receiverNum;

                                Log.e(TAG, "sendMsgToDB: " + String.valueOf(snap));
                                Log.e(TAG, "MsgCode has been set");
                                foundData[0]=true;

                                //Write message on user and chatmate database
                                if(setOnce[0]==false){
                                    chatRef.child(Singleton.getInstance().getCurrUid())
                                            .child("chatmsg").child(Singleton.getInstance().getChatUid())
                                            .child(msgCodeUser).setValue(messageToBeSent);
                                    chatRef.child(Singleton.getInstance().getChatUid())
                                            .child("chatmsg").child(Singleton.getInstance().getCurrUid())
                                            .child(msgCodeFriend).setValue(messageToBeSent);
                                    messageBox.setText("");
                                    setOnce[0]=true;
                                }
                            }
                        }
                    }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, TAG+ " onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");

    }


}
