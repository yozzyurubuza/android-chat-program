package com.example.xtrimitea.fragmenttransactions.SlidingTab_JsonRecyclerView;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.example.xtrimitea.fragmenttransactions.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity5 extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {

    private FragmentManager manager;
    private FragmentTransaction transaction;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stjrv_main);

        Singleton.getInstance().setFragmentManager(getSupportFragmentManager());
        manager = Singleton.getInstance().getFragmentManager();

//      1.1.1 Declare frag1 to be displayed in displayfrag1
        Act2_Frag1 frag1 = new Act2_Frag1();
        transaction = manager.beginTransaction();
        transaction.add(R.id.displayfrag1, frag1, "A");
        transaction.commit();

        JsonInsertData jsonInsertData = new JsonInsertData();
        jsonInsertData.main(null);

    }


    @Override
    public void onBackStackChanged() {

    }


}
