package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChatFragAddContact extends Fragment {
    private static final String TAG = ChatFragAddContact.class.getSimpleName();

    private TextView searchFullName;
    private EditText searchContactBox;
    private Button searchContactBut, addContactBut;
    private ImageView searchContactImage;

    private DatabaseReference userRef, chatRef;

    private Boolean emailFound = false;
    private String getSearchContact, getFname, getLname, getFullname, getImageURL, getCurrUid, getContactUid;
    private String[] friendList = new String[1000];
    private Bitmap[] bitmaps = new Bitmap[1000];
    private long getCount=0;
    private int numOfFriends = 0, contactNumOfFriends = 0;
    private ProgressDialog progressDialog;

    private List<String> friendsAdded = new ArrayList<String>();


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_frag_addcontact, container, false);
        Log.d(TAG, TAG+ " onCreateView");

        init(view);
        process(view);

        return view;
    }

    private void init(View view){
        searchContactBox = view.findViewById(R.id.searchContactBox);
        searchContactBut = view.findViewById(R.id.searchContactBut);
        searchContactImage = view.findViewById(R.id.searchContactImage);
        searchFullName = view.findViewById(R.id.searchFullName);
        addContactBut = view.findViewById(R.id.addContactBut);

        searchContactImage.setVisibility(View.INVISIBLE);
        searchFullName.setVisibility(View.INVISIBLE);
        addContactBut.setVisibility(View.INVISIBLE);

        userRef = FirebaseDatabase.getInstance().getReference().child("users");
        chatRef = FirebaseDatabase.getInstance().getReference().child("userfriends");

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Adding contact...");
        friendsAdded = Singleton.getInstance().getFriendsAdded();

    }

    private void process(View view){

        Singleton.getInstance().setInAddContact(true);

        final View view2 = view;
        final Boolean foundData[] = {false, false};
        searchContactBut.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                getSearchContact = searchContactBox.getText().toString();
                emailFound = false;
                bitmaps = Singleton.getInstance().getBitmapStore();
                Log.e(TAG, "bitmaps: "+Arrays.toString(bitmaps));
                Log.e(TAG, "NumOfFriends: "+Singleton.getInstance().getNumOfFriends());

                //Search email of contact..
                userRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot snap: dataSnapshot.getChildren()){
                            if(!foundData[0]) {
                                 if (snap.child("email").getValue().toString().equals(getSearchContact)) {
                                    Log.e(TAG, "Search Contact Snap: " + snap);

                                    //Get Fname, Lname, then combine
                                    getFname = snap.child("fname").getValue().toString();
                                    getLname = snap.child("lname").getValue().toString();
                                    getFullname = getFname + " "+ getLname;
                                    searchFullName.setText(getFullname);
                                    searchFullName.setVisibility(View.VISIBLE);

                                    //Get Image, download, then display
                                    getImageURL = snap.child("imageurl").getValue().toString();
                                    new Singleton.DownloadImageTask((ImageView) view2.findViewById(R.id.searchContactImage))
                                            .execute(getImageURL);
                                    searchContactImage.setVisibility(View.VISIBLE);

                                    //Get uid
                                    getCurrUid = Singleton.getInstance().getCurrUid();
                                    getContactUid = snap.getKey();
                                    Log.e(TAG, "getContactUid: "+getContactUid);

                                    //Search if the searched contact is already in the contact list
                                    friendList = Singleton.getInstance().getUserFriendList();
                                    Log.e(TAG, "Friend List: "+Arrays.toString(friendList));

                                    try{
                                        if(friendList[0].equals("")){
                                            Log.e(TAG, "User has friends");
                                        }
                                    }catch (Exception e){
                                        //friendList[0] = "asdasdsa";
                                        Log.e(TAG, "No friends found..");
                                    }

                                    try {
                                        for (int i = 0; i <= Singleton.getInstance().getNumOfFriends(); i++) {
                                            if (friendList[i].equals(getSearchContact)) {
                                                Log.e(TAG, "Searched contact is already in your list");
                                                i = Singleton.getInstance().getNumOfFriends();
                                                addContactBut.setVisibility(View.INVISIBLE);


                                            } else {
                                                Log.e(TAG, "Searching if searched contact is already in the contact list...");
                                                if (i == Singleton.getInstance().getNumOfFriends() && emailFound == false){
                                                    emailFound = true;
                                                    Log.e(TAG, "Not yet in the user's contact list, can be added");
                                                }
                                            }
                                        }
                                    }catch (Exception e){
                                        e.printStackTrace();
                                        Log.e(TAG, Arrays.toString(friendList));
                                        emailFound = true;
                                    }


                                     if (getSearchContact.equals(Singleton.getInstance().getCurrEmail())){
                                        Log.e(TAG, "Self user searched!");
                                        addContactBut.setVisibility(View.INVISIBLE);

                                    } else if (!getSearchContact.equals(Singleton.getInstance().getCurrEmail()) && emailFound == true) {
                                        //Add button appear
                                        addContactBut.setVisibility(View.VISIBLE);
                                        addContactBut.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                progressDialog.show();
                                                friendsAdded.add(getSearchContact);
                                                Singleton.getInstance().setFriendsAdded(friendsAdded);
                                                //Restart downloading all images of friends
                                                Singleton.getInstance().setDownloadImageFriends(false);
                                                Singleton.getInstance().setImageListFriendsPos(1);
                                                addContact();
                                                delayAdd();
                                            }
                                        });
                                    }

                                     //Store friends added in an arraylist
                                    for (int j=0; j<friendsAdded.size(); j++ ){
                                         if(getSearchContact.equals(friendsAdded.get(j))){
                                             addContactBut.setVisibility(View.INVISIBLE);
                                         }
                                     }

                                    foundData[0] = true;
                                    getCount = 0;

                                }  else {
                                    //No match found, hide widgets again
                                    searchFullName.setVisibility(View.INVISIBLE);
                                    searchContactImage.setVisibility(View.INVISIBLE);
                                    addContactBut.setVisibility(View.INVISIBLE);

                                    Log.e(TAG, "Searching Database for "+getSearchContact+"....");
                                    getCount++;
                                    if(getCount == dataSnapshot.getChildrenCount()){
                                        Log.e(TAG, "No match found!");
                                        Toast.makeText(getActivity(), "No match found!", Toast.LENGTH_SHORT).show();
                                        getCount = 0;
                                    }

                                }




                            }

                        }
                        foundData[0]=false;
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });
    }

    public void addContact(){

        Singleton.getInstance().setDownloadImageFriends(false);
        final Boolean insertOnce[]={false,false};
        //Search for the number of friends of contact to be added
        chatRef.child(getContactUid).child("friendlist").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                contactNumOfFriends = (int) dataSnapshot.getChildrenCount();
                Singleton.getInstance().setNumOfFriendsAdded(contactNumOfFriends+1);
                Log.e(TAG, "ContactUid Number of friends: "+ dataSnapshot.getChildrenCount());
                if (!insertOnce[0]){
                    chatRef.child(getContactUid).child("friendlist").child("friend"+Singleton.getInstance().getNumOfFriendsAdded())
                            .setValue(Singleton.getInstance().getCurrEmail());


                    insertOnce[0] = true;
                 }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //Search for the number of friends of current user
        chatRef.child(getCurrUid).child("friendlist").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                numOfFriends = (int) dataSnapshot.getChildrenCount();
                Singleton.getInstance().setNumOfFriends(numOfFriends+1);
                Log.e(TAG, "CurrUid Number of friends: " + dataSnapshot.getChildrenCount());
                if (!insertOnce[1]){
                    chatRef.child(getCurrUid).child("friendlist").child("friend"+Singleton.getInstance().getNumOfFriends())
                            .setValue(getSearchContact);
                    insertOnce[1] = true;
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public void delayAdd(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                addContactBut.setVisibility(View.INVISIBLE);
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Contact Added!", Toast.LENGTH_SHORT).show();
            }
        }, 3000);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Singleton.getInstance().setInAddContact(false);
        Log.d(TAG, TAG+ " onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");

    }


}
