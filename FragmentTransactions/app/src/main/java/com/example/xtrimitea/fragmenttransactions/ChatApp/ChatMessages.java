package com.example.xtrimitea.fragmenttransactions.ChatApp;

public class ChatMessages {
    private String from, message, type;
    private String sender, receiver;

    public ChatMessages(String message, String sender, String receiver) {
        this.from = from;
        this.message = message;
        this.sender = sender;
        this.receiver = receiver;

    }
    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
