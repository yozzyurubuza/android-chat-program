package com.example.xtrimitea.fragmenttransactions.ChatApp;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChatFragFlist extends Fragment {
    private static final String TAG = ChatFragFlist.class.getSimpleName();
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDataBaseRoot, mDatabaseUser, mDatabaseUserFriends;
    private FragmentManager manager;
    private FragmentTransaction transaction;
    private RecyclerView recyclerView;

    //Testing RecyclerView
    private JSONObject jsonObject, jsonObject2;
    private JSONArray jsonArray, jsonArray2;
    private JsonData jdata;
    private List<JsonData> items;
    private ListAdapter listAdapter;
    private String currEmail, getLname, getFname, getFullName, getImgURL;
    private String[] getFriendUID = new String[1000];
    private String[] fullName = new String[1000];
    private String[] friendImgURL = new String[1000];
    private String[] friendsUid = new String[1000];
    private int i,j, getCount=0;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_recycler_flist, container, false);
        Log.d(TAG, TAG+ " onCreateView");

        init(view);
        process();

        return view;
    }


    private void init(View view) {

        manager = getActivity().getSupportFragmentManager();
        firebaseAuth = FirebaseAuth.getInstance();
        mDataBaseRoot = FirebaseDatabase.getInstance().getReference().getRoot();
        mDatabaseUserFriends = FirebaseDatabase.getInstance().getReference().child("userfriends");
        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("users");

        currEmail = Singleton.getInstance().getCurrEmail();
        items = new ArrayList<>();

        recyclerView = view.findViewById(R.id.recyclerFriends);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

    }

    private void process() {

        //Sample JSON friends
//        try {
//            jsonObject = JsonInsertData.allJson2;
//            jsonArray = jsonObject.getJSONArray("VehicleTypes");
//            jsonObject2 = jsonArray.getJSONObject(0);
//            jsonArray2 = jsonObject2.getJSONArray("CarBrands");
//
//            items = new ArrayList<>();
//
//            for (int i = 0; i < jsonArray2.length(); i++) {
//                jdata = new JsonData(jsonArray2.getJSONObject(i).getString("CarBrand"));
//                items.add(jdata);
//            }
//
//            listAdapter = new ListAdapter(getChildFragmentManager(), items);
//            recyclerView.setAdapter(listAdapter);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        //JSON data directly parsed from firebase

        final Boolean[] foundData = {false,false};
        Log.e(TAG, currEmail);
        //Get friends of the current user
        mDatabaseUserFriends.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot val: dataSnapshot.getChildren()){
                    if(foundData[0] == false) {
                        if (val.child("email").getValue(String.class).contains(currEmail)) {
                            for (i = 0; i < val.child("friendlist").getChildrenCount(); i++) {
                                getFriendUID[i] = val.child("friendlist").child("friend" + String.valueOf(i + 1)).getValue(String.class);
                                //Put friend email to singleton string arrray
                                Singleton.getInstance().setFriendLists(getFriendUID);
                                Singleton.getInstance().setUserFriendList(getFriendUID);
                                //Get max number of friends
                                Singleton.getInstance().setNumOfFriends(i);
                                Log.e(TAG, Singleton.getInstance().getFriendLists(i)+" "+i);
                                Log.e(TAG, Singleton.getInstance().getNumOfFriends()+" ");
                            }
                            foundData[0] = true;
                            getCount = 0;

                        } else {
                            //User has no friends
                            Log.e(TAG, "Searching for friends...");
                            getCount++;
                            if (dataSnapshot.getChildrenCount() == getCount) {
                                Singleton.getInstance().setNumOfFriends(0);
                                Log.e(TAG, "User has no friends....");
                            }
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //Parse email from getFriendUID to get last name and first name of friends
        mDatabaseUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot2) {
                items.clear();
                for(DataSnapshot val2 : dataSnapshot2.getChildren()){
                    for(j=0; j<i;j++) {
                        if (val2.child("email").getValue(String.class).contains(Singleton.getInstance().getFriendLists(j))) {
                            getFname = val2.child("fname").getValue(String.class);
                            getLname = val2.child("lname").getValue(String.class);
                            getImgURL = val2.child("imageurl").getValue(String.class);
                            friendsUid[j] = val2.getKey();

                            Singleton.getInstance().setFriendsUid(friendsUid);
                            Log.e(TAG, "Friends UID List: "+ Arrays.toString(Singleton.getInstance().getFriendsUid()));



                            //Insert full name of friends to array
                            fullName[j] = getFname + " " + getLname;
                            friendImgURL[j] = getImgURL;
                            Log.e(TAG, "friendImgURL:"+Arrays.toString(friendImgURL));
                            Log.e(TAG, "fullName[j]: "+Arrays.toString(fullName));

                            Singleton.getInstance().setNewFriendImgURL(friendImgURL);
                            Singleton.getInstance().setNewFriendFullname(fullName);
                            Singleton.getInstance().setFriendListImages(friendImgURL);
                            Singleton.getInstance().setChatFriendFullName(fullName);
                            getFullName = Singleton.getInstance().getChatFriendFullName(j);
                            Log.e(TAG, "getFullName: "+getFullName);

                            //Inserting full name to recyclerview list
                            Log.e(TAG, getFullName + " "+ friendImgURL[j]);
                            jdata = new JsonData(fullName, friendImgURL[j]);
                            items.add(jdata);
                        }
                    }

                    Singleton.getInstance().setStoredList(items);
                    listAdapter = new ListAdapter(getActivity().getSupportFragmentManager(), items);
                    listAdapter.setOnClick(new ListAdapter.OnItemClicked() {
                        @Override
                        public void onItemClick(int position) {
                            Log.e(TAG, "RecyclerClickPos: "+String.valueOf(position));
                            Singleton.getInstance().setRecyclerClickPos(position);
                            position += 1;
                            Singleton.getInstance().setFriendNumDB("friend"+position);
                            Log.e(TAG, "FriendNumDB: "+Singleton.getInstance().getFriendNumDB());

                            Singleton.getInstance().setChatUid(friendsUid[position-1]);
                            Log.e(TAG, "Current Chatmate Uid: "+Singleton.getInstance().getChatUid());

                            //Open chat window with friend
                            manager = Singleton.getInstance().getManager();
                            transaction = manager.beginTransaction();

                            ChatFragChat chat = new ChatFragChat();
                            ChatFragFlist flist = (ChatFragFlist) manager.findFragmentByTag("Friends");
                            ChatFragHome home = (ChatFragHome) manager.findFragmentByTag("Home");

                            //Detach home and flist, then add new chat window
//                            transaction.detach(flist);
                            transaction.detach(home);
                            transaction.add(R.id.mainLayout, chat,"ChatWindow");
                            transaction.addToBackStack("addChatWindow");
                            transaction.commit();

                        }
                    });
                    recyclerView.setAdapter(listAdapter);

                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, TAG+ " onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");

    }


}
