package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ChatFragGroupMsgList extends Fragment {
    private static final String TAG = ChatFragGroupMsgList.class.getSimpleName();

    private ChatMessagesGC chatMessages;
    private List<ChatMessagesGC> messagesList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private GroupMsgListAdapter msgListAdapter;
    private RecyclerView recyclerMsg;
    private List<String> groupChatList = new ArrayList<>();

    private DatabaseReference groupRef, userRef;

    private String currUid, gcCode, chatCode, groupChatMsg, sender, receiver, from, fromImage;
    private int recyclerClickPos;
    private List<String> chatMembers = new ArrayList<String>();
    private List<String> groupFnameList = new ArrayList<String>();
    private List<String> groupImageList = new ArrayList<String>();
    private List<String> chatImageList = new ArrayList<String>();
    private List<String> groupUidList = new ArrayList<String>();
    private List<String> chatCodeList = new ArrayList<String>();
    private String[] gcCreator;
    private Boolean foundData = false;


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_recycler_flist, container, false);
        Log.d(TAG, TAG+ " onCreateView");

        init(view);
        process(view);

        return view;
    }

    private void init(View view){

        groupRef = FirebaseDatabase.getInstance().getReference().child("groupchats");
        userRef = FirebaseDatabase.getInstance().getReference().child("users");

        currUid = Singleton.getInstance().getCurrUid();
        recyclerClickPos = Singleton.getInstance().getRecyclerClickPos();
        gcCreator = Singleton.getInstance().getGcCreator();

        recyclerMsg = view.findViewById(R.id.recyclerFriends);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setStackFromEnd(true);
        recyclerMsg.setLayoutManager(linearLayoutManager);
        groupChatList = Singleton.getInstance().getGroupChatList();
    }

    private void process(View view){
        //Parse Members of GC first
        groupRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                chatMembers.clear();
                for(DataSnapshot snap: dataSnapshot.getChildren()){
                    if (snap.getKey().equals(currUid)){

                        gcCode = groupChatList.get(recyclerClickPos-1);


                        Log.e(TAG, "GcCode: "+gcCode);
                        Log.e(TAG, "Snap data: "+snap);
                        Log.e(TAG, "GC Member Count: "+snap.child(gcCode).child("members").getChildrenCount());

                        for(int i=0; i<snap.child(gcCode).child("members").getChildrenCount(); i++){

                            chatMembers.add(snap.child(gcCode).child("members").child("member"+i).getValue(String.class));
                        }

                        for (int j = 0; j<snap.child(gcCode).child("members").getChildrenCount(); j++){
                            Log.e(TAG, "Chat Member #"+j+": "+chatMembers.get(j));
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //GET - Chat Members Fname
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                groupFnameList.clear();
                for(DataSnapshot snap: dataSnapshot.getChildren()){
                    for(int i=0; i<chatMembers.size(); i++){
                        if(snap.getKey().equals(chatMembers.get(i))) {
                            Log.e(TAG, "Chat Member #"+i+": "+chatMembers.get(i));
                            String fname = snap.child("fname").getValue(String.class);
                            groupFnameList.add(fname+"_"+chatMembers.get(i));
                            Log.e(TAG, "fname added: " + fname);
                        }
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //GET - Chat Members Images from array
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                groupImageList.clear();
                for(DataSnapshot snap: dataSnapshot.getChildren()){
                    for(int i=0; i<chatMembers.size(); i++){
                        if(snap.getKey().equals(chatMembers.get(i))) {
                            String imageurl = snap.child("imageurl").getValue(String.class);
                            groupImageList.add(imageurl+"_"+chatMembers.get(i));
                            Log.e(TAG, "Group Member Image added: " + imageurl+"_"+chatMembers.get(i));
                        }
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        //GET - Parse message sender
        groupRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                messagesList.clear();
                chatImageList.clear();
                groupUidList.clear();
                chatCodeList.clear();
                for(DataSnapshot snap: dataSnapshot.getChildren()){
                    if(snap.getKey().equals(currUid)){
                        snap = snap.child(gcCode).child("chatmsg");
                        Log.e(TAG, "Get group messages: "+snap);

                        //Parse the messages from DB
                        for (int i=0; i<1000; i++){
                            for (int j=0; j<chatMembers.size(); j++) {
                                //Parse who is the sender of message
                                chatCode = "msg_" + chatMembers.get(j) + "_" + i;
                                groupChatMsg = snap.child(chatCode).getValue(String.class);

                                if (groupChatMsg != null){
                                    Log.e(TAG, "Chat Message from "+chatCode+": "+ groupChatMsg);
                                    chatCodeList.add(chatCode);
                                    if (chatMembers.get(j).equals(currUid)){

                                        // GET the fname and image from the uid of the sender
                                        // Find name of the sender
                                        for (int k=0; k<chatMembers.size(); k++){
                                            Log.e(TAG, "(Sender) Searching...");
                                            String fnames = groupFnameList.get(k);
                                            String findFname = fnames
                                                    .replace("_"+chatMembers.get(j),"");
                                            if (!findFname.equals(fnames)){
                                                from = findFname;
                                                Log.e(TAG, "Message sender found!: "+from);

                                                //Find image of the message sender
                                                for (int l=0; l<groupImageList.size(); l++){
                                                    String images = groupImageList.get(l);
                                                    String findImages = images.replace("_"+chatMembers.get(j),"");
                                                    if(!findImages.equals(images)){
                                                        fromImage = findImages;
                                                        Log.e(TAG, "Image sender found!: "+ fromImage);
                                                        chatImageList.add(fromImage);
                                                        Log.e(TAG, "Image sender uid: "+chatMembers.get(j));
                                                        groupUidList.add(chatMembers.get(j));
                                                        l = groupImageList.size();
                                                    }

                                                }
                                                k = chatMembers.size();
                                            }
                                        }
                                        sender = groupChatMsg;
                                        receiver = "null";
                                        chatMessages = new ChatMessagesGC(from, sender, sender, receiver, fromImage);
                                    } else {

                                        // Parse Other Contacts name
                                        for (int k=0; k<chatMembers.size(); k++){
                                            Log.e(TAG, "(Receiver) Searching...");
                                            String fnames = groupFnameList.get(k);
                                            String findFname = fnames
                                                    .replace("_"+chatMembers.get(j),"");
                                            if (!findFname.equals(fnames)){
                                                from = findFname;
                                                Log.e(TAG, "Message sender found!: "+from);

                                                //Find image of the message sender
                                                for (int l=0; l<groupImageList.size(); l++) {
                                                    String images = groupImageList.get(l);
                                                    String findImages = images.replace("_" + chatMembers.get(j), "");
                                                    if (!findImages.equals(images)) {
                                                        fromImage = findImages;
                                                        Log.e(TAG, "Image sender found!: " + fromImage);
                                                        chatImageList.add(fromImage);
                                                        Log.e(TAG, "Image sender uid: "+chatMembers.get(j));
                                                        groupUidList.add(chatMembers.get(j));
                                                        l = groupImageList.size();
                                                    }
                                                }
                                                k = chatMembers.size();
                                            }
                                        }


                                        sender = "null";
                                        receiver = groupChatMsg;
                                        chatMessages = new ChatMessagesGC(
                                                from, receiver, sender, receiver, fromImage);
                                    }
                                    messagesList.add(chatMessages);
                                }
                            }
                        }
                        Log.e(TAG, "MessageListSize: "+messagesList.size());
                        Log.e(TAG, "ChatImgListSize: "+chatImageList.size());
                        Singleton.getInstance().setChatMembersList(chatMembers);
                        Singleton.getInstance().setGcImageArray(chatImageList);
                        Singleton.getInstance().setGcMsgSize(chatImageList.size());
                        Log.e(TAG, "MsgSize: "+Singleton.getInstance().getGcMsgSize());
                        msgListAdapter = new GroupMsgListAdapter(getActivity().getSupportFragmentManager(), messagesList);
                        recyclerMsg.setAdapter(msgListAdapter);
                        msgListAdapter.setOnLongPress(new GroupMsgListAdapter.OnLongPress() {
                            @Override
                            public void onLongPress(int position) {
                                //Toast.makeText(getActivity(), "Message Code: "+chatCodeList.get(position), Toast.LENGTH_SHORT).show();
                                optionDialog(chatCodeList.get(position));
                            }
                        });


                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        //Parse Group Message
//        groupRef.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                messagesList.clear();
//                for(DataSnapshot snap: dataSnapshot.getChildren()){
//                    if (snap.getKey().equals(currUid)) {
//                        snap = snap.child("chatmsg");
//                        Log.e(TAG, "GC Messages: "+snap);
//                        for(int i=0; i<=snap.getChildrenCount();i++) {
//                            int j = 1, k = 2;
//                            if (snap.child("msg"+i+"_"+j).getValue() != null){
//                                String sender = snap.child("msg"+i+"_"+j).getValue(String.class);
//                                String receiver = "null";
//                                Log.e(TAG, "Sender message found! "+sender);
//                                chatMessages = new ChatMessages(sender, sender, receiver);
//                                messagesList.add(chatMessages);
//                            }
//                            else if (snap.child("msg"+i+"_"+k).getValue() != null){
//                                String sender = "null";
//                                String receiver = snap.child("msg"+i+"_"+k).getValue(String.class);
//                                Log.e(TAG, "Receiver message found! "+receiver);
//                                chatMessages = new ChatMessages(receiver, sender, receiver);
//                                messagesList.add(chatMessages);
//                            }
//                        }
//                    }
//                }
//                msgListAdapter = new MsgListAdapter(getChildFragmentManager(), messagesList);
//                recyclerMsg.setAdapter(msgListAdapter);
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

    }

    public void optionDialog(final String code){
        //Parse owner of message

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        //Dialog Box Yes or No

        // add a list
        String[] options = {"Copy", "Delete"};
        builder.setTitle("Select Action");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(which == 0){
                    Toast.makeText(getActivity(), "Message added to clipboard!", Toast.LENGTH_SHORT).show();
                }
                else if (which == 1){
                    //Delete message from all members
                    for(int i=0; i<chatMembers.size(); i++){
                        String memberUid = chatMembers.get(i);
                        groupRef.child(memberUid).child(gcCode).child("chatmsg")
                                .child(code).removeValue();
                    }

                    Toast.makeText(getActivity(), "Message deleted", Toast.LENGTH_SHORT).show();
                }

            }
        });

        builder.setIcon(android.R.drawable.ic_menu_help)
                .show();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, TAG+ " onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");

    }


}

