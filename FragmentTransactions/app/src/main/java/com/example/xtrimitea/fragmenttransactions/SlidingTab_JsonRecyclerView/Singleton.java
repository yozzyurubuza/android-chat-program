package com.example.xtrimitea.fragmenttransactions.SlidingTab_JsonRecyclerView;


import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

//1
public class Singleton {

    private static Singleton sInstance = new Singleton();

    public static Singleton getInstance(){
        if (sInstance == null){
            sInstance = new Singleton();
        }
        return sInstance;
    }

    //Strings
    private String vehicleTypes;
    private String carBrands;
    private ArrayList items;
    private ArrayList itemsBot;
    private String carBrand1, carBrand2, carBrand3;

    private int fragChilPos;
    private int mainFragPos;

    private JSONObject jsonObject;
    private JSONArray jsonArray;
    private JSONObject jsonObjectBot;
    private JSONArray jsonArrayBot;

    //Objects
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private RecyclerView.LayoutManager layoutManager;
    private JsonData jdata;

    private TabLayout tabLayout;

    public TabLayout getTabLayout() {
        return tabLayout;
    }

    public void setTabLayout(TabLayout tabLayout) {
        this.tabLayout = tabLayout;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public JSONObject setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
        return jsonObject;
    }

    public String getCarBrand1() {
        return carBrand1;
    }

    public String setCarBrand1(String carBrand1) {
        this.carBrand1 = carBrand1;
        return carBrand1;
    }

    public String getCarBrand2() {
        return carBrand2;
    }

    public String setCarBrand2(String carBrand2) {
        this.carBrand2 = carBrand2;
        return carBrand2;
    }

    public String getCarBrand3() {
        return carBrand3;
    }

    public String setCarBrand3(String carBrand3) {
        this.carBrand3 = carBrand3;
        return carBrand3;
    }

    public JSONArray getJsonArray() {
        return jsonArray;
    }

    public JSONArray setJsonArray(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
        return jsonArray;
    }

    public String getVehicleTypes() {
        return vehicleTypes;
    }

    public String setVehicleTypes(String vehicleTypes) {
        this.vehicleTypes = vehicleTypes;
        return vehicleTypes;
    }

    public String getCarBrands() {
        return carBrands;
    }

    public void setCarBrands(String carBrands) {
        this.carBrands = carBrands;
    }

    public ArrayList getItems() {
        return items;
    }

    public List<JsonData> setItems(ArrayList items) {
        this.items = items;
        return null;
    }

    public JSONObject getJsonObjectBot() {
        return jsonObjectBot;
    }

    public void setJsonObjectBot(JSONObject jsonObjectBot) {
        this.jsonObjectBot = jsonObjectBot;
    }

    public JSONArray getJsonArrayBot() {
        return jsonArrayBot;
    }

    public void setJsonArrayBot(JSONArray jsonArrayBot) {
        this.jsonArrayBot = jsonArrayBot;
    }

    public ArrayList getItemsBot() {
        return itemsBot;
    }

    public void setItemsBot(ArrayList itemsBot) {
        this.itemsBot = itemsBot;
    }



    // Objects

    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public FragmentTransaction getFragmentTransaction() {
        return fragmentTransaction;
    }

    public void setFragmentTransaction(FragmentTransaction fragmentTransaction) {
        this.fragmentTransaction = fragmentTransaction;
    }

    public RecyclerView.LayoutManager getLayoutManager() {
        return layoutManager;
    }

    public RecyclerView.LayoutManager setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        this.layoutManager = layoutManager;
        return layoutManager;
    }

    public JsonData getJdata() {
        return jdata;
    }

    public JsonData setJdata(JsonData jdata) {
        this.jdata = jdata;
        return jdata;
    }

    //Layouts


    public int getMainFragPos() {
        return mainFragPos;
    }

    public void setMainFragPos(int mainFragPos) {
        this.mainFragPos = mainFragPos;
    }


    public int getFragChilPos() {
        return fragChilPos;
    }

    public void setFragChilPos(int fragChilPos) {
        this.fragChilPos = fragChilPos;
    }


}


