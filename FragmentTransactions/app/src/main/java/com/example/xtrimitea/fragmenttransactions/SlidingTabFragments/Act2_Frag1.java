package com.example.xtrimitea.fragmenttransactions.SlidingTabFragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xtrimitea.fragmenttransactions.R;

public class Act2_Frag1 extends Fragment {

    public static TabLayout tabLayout, tabLayout2;
    public static ViewPager viewPager, viewPager2;
    public static PagerAdapter pagerAdapter, pagerAdapter2;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("TAG","Act_Frag1 onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("TAG","Act_Frag1 onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.act2_frag1, container, false);
        Log.d("TAG","Act_Frag1 onCreateView");

        // First tabLayout
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("Tab 1"));
        tabLayout.addTab(tabLayout.newTab().setText("Tab 2"));
        tabLayout.addTab(tabLayout.newTab().setText("Tab 3"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        pagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager(), tabLayout.getTabCount(),0);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("TAG","Act_Frag1 onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TAG","Act_Frag1 onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TAG","Act_Frag1 onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("TAG","Act_Frag1 onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TAG","Act_Frag1 onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("TAG","Act_Frag1 onDetach");
    }
}
