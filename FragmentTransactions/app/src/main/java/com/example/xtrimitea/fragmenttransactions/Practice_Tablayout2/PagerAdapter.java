package com.example.xtrimitea.fragmenttransactions.Practice_Tablayout2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.xtrimitea.fragmenttransactions.FragmentTransactions.FragmentA;
import com.example.xtrimitea.fragmenttransactions.FragmentTransactions.FragmentB;

//How to create a simple tabs with fragment and viewpager
//1. Create Fragment class and layout (See FragmentA and B)
//2. Create PagerAdapter class
//3. Create layout for the main activity using toolbar, tablayout, and viewpager (See activity_main4.xml)

//2.1 Extend to FragmentStatePagerAdapter and implement methods and super
public class PagerAdapter extends FragmentStatePagerAdapter{
    int mNumOfTabs;

    //super
    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    //methods
    @Override
    public Fragment getItem(int position) {

        //2.2 Create condition statement for each tabs
        if (position == 0){
            FragmentA fragmentA = new FragmentA();
            return fragmentA;
        }
        else if (position == 1) {
            FragmentB fragmentB = new FragmentB();
            return fragmentB;
        }
        else if (position == 2){
            FragmentA fragmentA = new FragmentA();
            return fragmentA;
        }
        else
            return null;
    }

    @Override
    public int getCount() {
        //2.3 Return the number of tabs
        return mNumOfTabs;
    }
}
