package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xtrimitea.fragmenttransactions.R;

public class ChatFragSettings extends Fragment {
    private static final String TAG = ChatFragSettings.class.getSimpleName();
    ConstraintLayout option1, option2, option3, option4, option1_1;
    FragmentManager manager;
    FragmentTransaction transaction;
    ProgressDialog progressDialog;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_frag_settings, container, false);
        Log.d(TAG, TAG+ " onCreateView");

        init(view);
        process(view);

        return view;
    }

    private void init(View view){
        option1 = view.findViewById(R.id.option1);
        option1_1 = view.findViewById(R.id.option1_1);
        option2 = view.findViewById(R.id.option2);
        option3 = view.findViewById(R.id.option3);
        option4 = view.findViewById(R.id.option4);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Logging Out....");

    }

    private void process(View view){
        option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Call Add Friend Fragment
                addContactFrag();

            }
        });

        option1_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create Group Chat
                createGCFrag();
            }
        });

        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Call Upload Image Fragment
                addUploadImgFrag();
            }
        });

        option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Call Change User Settings Fragment
                changeUserSet();
            }
        });

        option4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Start logout dialog
                logoutDialog();
                progressDialog.hide();

            }
        });

    }

    public void addFrag(){
        ChatFragMsgList msgList = new ChatFragMsgList();
        manager = Singleton.getInstance().getManager();
        transaction = manager.beginTransaction();
        transaction.add(R.id.scrollChatView, msgList, "Messages");
        transaction.commit();
        Log.d(TAG, "addFrag: in "+TAG);
    }



    public void addContactFrag(){
        ChatFragAddContact addContact = new ChatFragAddContact();

        manager = Singleton.getInstance().getManager();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.mainLayout, addContact, "AddContact");
        transaction.addToBackStack("AddContact");
        transaction.commit();
        Log.d(TAG, "Add Contact Fragment");

    }

    public void createGCFrag(){
        ChatFragCreateGC createGC = new ChatFragCreateGC();

        manager = Singleton.getInstance().getManager();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.mainLayout, createGC, "Create GC");
        transaction.addToBackStack("CreateGC");
        transaction.commit();
        Log.d(TAG, "Add Create GC Fragment");

    }


    public void addUploadImgFrag(){
        ChatFragUploadImage uploadImage = new ChatFragUploadImage();

        manager = Singleton.getInstance().getManager();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.mainLayout, uploadImage, "UploadImage");
        transaction.addToBackStack("UploadImage");
        transaction.commit();
        Log.d(TAG, "Add Upload Image Fragment");

    }

    public void changeUserSet(){
        ChatFragChangeUserSet changeUserSet = new ChatFragChangeUserSet();

        manager = Singleton.getInstance().getManager();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.mainLayout, changeUserSet, "ChangeUserSettings");
        transaction.addToBackStack("ChangeUserSettings");
        transaction.commit();
        Log.d(TAG, "Add Change User Settings Fragment");
    }

    //Warning Dialog for Logout
    public void logoutDialog(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        //Dialog Box Yes or No
        builder.setTitle("Logout")
                .setMessage("Are you sure you want to Logout?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        progressDialog.show();
                        //Clear user data
                        Singleton.getInstance().clearData();

                        //Call Login Fragment function
                        ChatFragLogin login = new ChatFragLogin();
                        manager = Singleton.getInstance().getManager();
                        transaction = manager.beginTransaction();
                        transaction.replace(R.id.mainLayout, login, "Login" );
                        manager.popBackStack();
                        transaction.commit();
                        Log.e(TAG, "Logging Out....");

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }




    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, TAG+ " onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        progressDialog.dismiss();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");

    }


}
