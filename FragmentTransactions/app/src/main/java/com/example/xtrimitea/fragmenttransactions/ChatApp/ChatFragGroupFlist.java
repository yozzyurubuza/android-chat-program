package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChatFragGroupFlist extends Fragment {
    private static final String TAG = ChatFragGroupFlist.class.getSimpleName();


    private DatabaseReference groupRef, userRef;
    private RecyclerView recyclerGCList;
    private JsonData jdata;
    private List<JsonData> itemsGC = new ArrayList<>();
    private GCListAdapter gcListAdapter;

    private String[] gcName, gcImage, gcCode, gcCreator;
    private String currUid;
    private Bitmap[] gcImageList;

    private Boolean foundData = false;
    private FragmentManager manager;
    private FragmentTransaction transaction;

    private String groupChatID, userID;
    private List<String> groupChatList = new ArrayList<String>();
    private List<String> userList = new ArrayList<String>();
    private int y;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_recycler_flist, container, false);
        Log.d(TAG, TAG+ " onCreateView");

        init(view);
        process(view);

        return view;
    }

    private void init(View view){
        gcImage = Singleton.getInstance().getGcImage();
        gcName = Singleton.getInstance().getGcName();
        currUid = Singleton.getInstance().getCurrUid();
        gcCode = Singleton.getInstance().getGcCode();
        gcCreator = Singleton.getInstance().getGcCreator();

        groupRef = FirebaseDatabase.getInstance().getReference().child("groupchats");
        userRef = FirebaseDatabase.getInstance().getReference().child("users");

        recyclerGCList = view.findViewById(R.id.recyclerFriends);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerGCList.setLayoutManager(layoutManager);
        y = Singleton.getInstance().getGroupChatFlist();

    }

    private void process(View view){

        //Retrieve all user to get the group chat creator
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList.clear();
                for(DataSnapshot snap: dataSnapshot.getChildren()){
                    userList.add(snap.getKey());
                    Log.e(TAG, "Userlist added: "+snap.getKey());
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        groupRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snap : dataSnapshot.getChildren()) {
                    if (!foundData){
                        if(snap.getKey().equals(currUid)) {
                            itemsGC.clear();
                            groupChatList.clear();
                            y = 0;
                            Log.e(TAG, "I'm in!");
                            Log.e(TAG, snap.getChildrenCount()+"");
                            //for (int i = 0; i < snap.getChildrenCount(); i++) {
                                //Get all Group Chat data of the current user and store it in an array
                                //Parse group chat creator
                                for (int x=0; x<userList.size();x++){
                                    userID = userList.get(x);

                                    for (int z=0; z<100; z++) {
                                        //Log.e(TAG, "gc_" + userID + "_" + String.valueOf(z + 1));
                                        String gcCodeName = snap.child("gc_" + userID + "_" + String.valueOf(z + 1))
                                                .child("gcname").getValue(String.class);
                                        String gcCodeImage = snap.child("gc_" + userID + "_" + String.valueOf(z + 1))
                                                .child("imageurl").getValue(String.class);
                                        if (gcCodeName != null) {
                                            gcName[y] = gcCodeName;
                                            gcImage[y] = gcCodeImage;
                                            gcCode[y] = snap.child("gc_" + userID + "_" + String.valueOf(z + 1)).getKey();
                                            groupChatList.add(gcCode[y]);
                                            Log.e(TAG, "Adding to groupChatList: "+gcCode[y]);
                                            gcCreator[y] = userID;
                                            //x = userList.size();
                                            Log.e(TAG, "GC Creator Found: " + gcName[y] + " " + gcImage[y]);
                                            Log.e(TAG, "GC Code: " + gcCode[y]);
                                            y+=1;


                                        }
                                    }

                                }

                                Singleton.getInstance().setGcImage(gcImage);
                                Singleton.getInstance().setGcName(gcName);
                                Singleton.getInstance().setGcCode(gcCode);
                                Singleton.getInstance().setGcCreator(gcCreator);
                                Singleton.getInstance().setGroupChatList(groupChatList);

                                Log.e(TAG, "GcCreator Array: "+ Arrays.toString(gcCreator));
                                Log.e(TAG, "gcName Array: " + Arrays.toString(gcName));
                                Log.e(TAG, "gcImage Array: " + Arrays.toString(gcImage));
                                Log.e(TAG, "GroupChatFlist: "+y);


                            for (int i=0; i<groupChatList.size(); i++) {
                                Log.e(TAG, "groupChatList size: " +groupChatList.size());
                                jdata = new JsonData(gcName, gcImage[i]);
                                itemsGC.add(jdata);
                                Singleton.getInstance().setItemsGC(itemsGC);
                            }

//                                jdata = new JsonData(gcName, gcImage[i]);
//                                itemsGC.add(jdata);
//                                Singleton.getInstance().setItemsGC(itemsGC);

                            //}
                            foundData = true;

                            gcListAdapter = new GCListAdapter(getActivity().getSupportFragmentManager(), itemsGC);
                            gcListAdapter.setOnClick(new GCListAdapter.OnItemClicked() {
                                @Override
                                public void onItemClick(int position) {
                                    Singleton.getInstance().setCurrentGCname(gcName[position]);
                                    //Log.e(TAG, gcName[position]);
                                    gcImageList = Singleton.getInstance().getGcImageList();
                                    Singleton.getInstance().setCurrentGCdp(gcImageList[position]);
                                    //Log.e(TAG, gcImageList[position]+"");
                                    Singleton.getInstance().setRecyclerClickPos(position+1);
                                    startGC();
                                }
                            });
                        }
                    }
                }
                recyclerGCList.setAdapter(gcListAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        foundData = false;
    }

    public void startGC(){
        ChatFragGroupChat groupChat = new ChatFragGroupChat();
        manager = Singleton.getInstance().getManager();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.mainLayout, groupChat, "Group Messages");
        transaction.addToBackStack("Group Messages");
        transaction.commit();
        Log.d(TAG, "addFrag: in "+TAG);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
        Singleton.getInstance().setGroupChatFlist(0);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, TAG+ " onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");

    }


}
