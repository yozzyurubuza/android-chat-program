package com.example.xtrimitea.fragmenttransactions.ChatApp;

public class UploadToFirebase {
    private String mName;
    private String mImageUrl;

    public UploadToFirebase(){

    }

    public UploadToFirebase(String name, String imageUrl ){
        if(name.trim().equals("")){
            name = "No Name";
        }
        mName = name;
        mImageUrl = imageUrl;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }
}
