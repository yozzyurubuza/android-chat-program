package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.app.ProgressDialog;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.xtrimitea.fragmenttransactions.R;

public class ChatActivityMainActivity extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {

    private FragmentManager manager;
    private FragmentTransaction transaction;
    private static final String TAG = ChatActivityMainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity_main);

        //Set progress messages
        progressMessages();

        //Initiate Login Fragment
        ChatFragLogin chatFragLogin = new ChatFragLogin();
        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.add(R.id.mainLayout, chatFragLogin, "Login");
        transaction.addToBackStack("addLogin");
        transaction.commit();
        Singleton.getInstance().setManager(manager);

    }

    @Override
    public void onBackStackChanged() {
        int count = manager.getBackStackEntryCount();
        for (int i=count-1;i>=0;i--){
            FragmentManager.BackStackEntry entry = manager.getBackStackEntryAt(i);
            Log.e(TAG, TAG+ " "+ entry.getName());
        }

    }

    public void progressMessages(){
        Singleton.getInstance().setProgressReg("Registering....");
    }
}
