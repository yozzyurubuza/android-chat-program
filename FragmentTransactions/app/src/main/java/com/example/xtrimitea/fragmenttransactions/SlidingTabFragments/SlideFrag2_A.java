package com.example.xtrimitea.fragmenttransactions.SlidingTabFragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xtrimitea.fragmenttransactions.R;

public class SlideFrag2_A extends Fragment {
    public static int mFragNum = 0;
    private TabLayout tabLayoutA;
    private ViewPager viewPagerA;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("TAG","SlideFrag2_A onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("TAG","SlideFrag2_A onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view,view2;
        //view = inflater.inflate(R.layout.fragment_a, container, false);
        view2 = inflater.inflate(R.layout.fragment_b1, container, false);

        Log.d("TAG","SlideFrag2_A onCreateView");
        return view2;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("TAG","SlideFrag2_A onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TAG","SlideFrag2_A onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TAG","SlideFrag2_A onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("TAG","SlideFrag2_A onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TAG","SlideFrag2_A onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("TAG","SlideFrag2_A onDetach");
    }
}
