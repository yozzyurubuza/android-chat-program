package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Arrays;
import java.util.List;

public class MsgListAdapter extends RecyclerView.Adapter<MsgListAdapter.ViewHolder> {
    private FragmentManager context;
    private List<ChatMessages> items;
    private FirebaseAuth auth;
    private DatabaseReference ref;
    private String TAG = MsgListAdapter.class.getSimpleName();
    private int recyclerPos;
    private Bitmap[] bitmapStore = new Bitmap[1000];


    public MsgListAdapter(FragmentManager context,List<ChatMessages> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_recycler_msgformat, parent, false);

        auth = FirebaseAuth.getInstance();
        ref = FirebaseDatabase.getInstance().getReference().child("userfriends");

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        recyclerPos = Singleton.getInstance().getRecyclerClickPos();
        bitmapStore = Singleton.getInstance().getBitmapStore();
        holder.image.setImageBitmap(bitmapStore[recyclerPos+1]);
        Log.e(TAG, "bitmapStore Array: "+ Arrays.toString(bitmapStore));
        Log.e(TAG, "Setting image for chat: "+bitmapStore[recyclerPos+1] + " / "+String.valueOf(recyclerPos+1));

        holder.senderTxt.setText(items.get(position).getSender());
        holder.receiverTxt.setText(items.get(position).getReceiver());

        if(holder.senderTxt.getText().toString().equals("null")){
            holder.senderTxt.setVisibility(View.INVISIBLE);
            holder.receiverTxt.setVisibility(View.VISIBLE);
            holder.image.setVisibility(View.VISIBLE);
        }
        else if (holder.receiverTxt.getText().toString().equals("null")){
            holder.receiverTxt.setVisibility(View.INVISIBLE);
            holder.image.setVisibility(View.INVISIBLE);
            holder.senderTxt.setVisibility(View.VISIBLE);
        }
        else{
            Log.e(TAG, "No null");
        }

    }




    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private AsyncTask<String, Void, Bitmap> image1;
        private TextView senderTxt;
        private TextView receiverTxt;
        private ConstraintLayout chatLayout;
        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            image =  itemView.findViewById(R.id.receiverImageMsg);

            senderTxt = itemView.findViewById(R.id.senderMsg);
            receiverTxt = itemView.findViewById(R.id.receiverMsg);
            chatLayout = itemView.findViewById(R.id.chatLayout);
        }
    }


}
