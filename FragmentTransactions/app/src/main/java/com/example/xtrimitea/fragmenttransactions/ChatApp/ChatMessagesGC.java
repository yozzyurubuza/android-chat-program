package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.graphics.Bitmap;

public class ChatMessagesGC {
    private String from, message, type;
    private String sender, receiver;
    private String imageurl;

    public ChatMessagesGC(String from, String message, String sender, String receiver, String imageurl) {
        this.from = from;
        this.message = message;
        this.sender = sender;
        this.receiver = receiver;
        this.imageurl = imageurl;

    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
