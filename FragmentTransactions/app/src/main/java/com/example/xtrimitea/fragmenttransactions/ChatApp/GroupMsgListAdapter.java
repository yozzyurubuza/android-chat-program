package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class GroupMsgListAdapter extends RecyclerView.Adapter<GroupMsgListAdapter.ViewHolder> {
    private FragmentManager context;
    private List<ChatMessagesGC> items;
    private String TAG = GroupMsgListAdapter.class.getSimpleName();


    private GroupMsgListAdapter.OnItemClicked onClick;
    private GroupMsgListAdapter.OnLongPress longPress;

    public interface OnLongPress{
        void onLongPress(int position);
    }

    public interface OnItemClicked{
        void onItemClick(int position);
    }


    public GroupMsgListAdapter(FragmentManager context, List<ChatMessagesGC> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_recycler_gc_msgformat, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        //Parsing GC Members Image List

        holder.senderTxt.setText(items.get(position).getSender());
        holder.receiverTxt.setText(items.get(position).getReceiver());
        holder.fname.setText(items.get(position).getFrom());

        new Singleton.DownloadImageTask((ImageView) holder.itemView.findViewById(R.id.receiverImageMsgGC))
                .execute(items.get(position).getImageurl());


        if(holder.senderTxt.getText().toString().equals("null")){
            holder.fname.setVisibility(View.VISIBLE);
            holder.senderTxt.setVisibility(View.INVISIBLE);
            holder.receiverTxt.setVisibility(View.VISIBLE);
            holder.image.setVisibility(View.VISIBLE);
        }
        else if (holder.receiverTxt.getText().toString().equals("null")){
            holder.receiverTxt.setVisibility(View.INVISIBLE);
            holder.image.setVisibility(View.INVISIBLE);
            holder.senderTxt.setVisibility(View.VISIBLE);
            holder.fname.setVisibility(View.INVISIBLE);
        }
        else{
            Log.e(TAG, "No null");
        }

        holder.chatLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                longPress.onLongPress(position);
                //Toast.makeText(view.getContext(), "Message #"+position+":", Toast.LENGTH_SHORT).show();
                return true;
            }
        });


    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        private AsyncTask<String, Void, Bitmap> image2;
        private TextView senderTxt;
        private TextView receiverTxt;
        private TextView fname;
        private ConstraintLayout chatLayout;
        private ImageView image;
        private int pos;
        private List<String> gcImageArray = new ArrayList<String>();
        private Boolean setOnce;

        public ViewHolder(View itemView) {
            super(itemView);

            image =  itemView.findViewById(R.id.receiverImageMsgGC);

            fname = itemView.findViewById(R.id.receiverFName);
            senderTxt = itemView.findViewById(R.id.senderMsgGC);
            receiverTxt = itemView.findViewById(R.id.receiverMsgGC);
            chatLayout = itemView.findViewById(R.id.chatLayoutGC);


        }

        @Override
        public boolean onLongClick(View view) {


            Toast.makeText(view.getContext(), "Long pressed msg# ", Toast.LENGTH_SHORT).show();


            return true;
        }
    }

    public void setOnClick(GroupMsgListAdapter.OnItemClicked onClick){
        this.onClick=onClick;
    }

    public void setOnLongPress(GroupMsgListAdapter.OnLongPress longPress){
        this.longPress = longPress;
    }


}












