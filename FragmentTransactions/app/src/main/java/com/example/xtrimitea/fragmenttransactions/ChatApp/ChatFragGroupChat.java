package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ChatFragGroupChat extends Fragment {
    private static final String TAG = ChatFragGroupChat.class.getSimpleName();

    private Button backButGC, settingsGC, sendButGC;
    private EditText messageBoxGC;
    private ImageView gcDP;
    private TextView gcName;

    private FragmentManager manager;
    private FragmentTransaction transaction;

    private DatabaseReference groupRef;

    private String groupChatMsg, currUid, currGcCode, chatCode;
    private List<String> chatMembers = new ArrayList<String>();
    private List<String> messageList = new ArrayList<String>();
    private int recyclerClickPos, totalMsgs;
    private String[] gcCode;
    private int lastIndex;

    private String lastIndexKey;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_frag_window_gc, container, false);
        Log.d(TAG, TAG+ " onCreateView");

        init(view);
        process(view);

        return view;
    }

    private void init(View view){
        backButGC = view.findViewById(R.id.backButGC);
        settingsGC = view.findViewById(R.id.settingsGC);
        sendButGC = view.findViewById(R.id.sendButGC);
        messageBoxGC = view.findViewById(R.id.messageBoxGC);
        gcDP = view.findViewById(R.id.gcDP);
        gcName = view.findViewById(R.id.gcName);

        groupRef = FirebaseDatabase.getInstance().getReference().child("groupchats");

        currUid = Singleton.getInstance().getCurrUid();
        recyclerClickPos = Singleton.getInstance().getRecyclerClickPos();

        gcCode = Singleton.getInstance().getGcCode();
        currGcCode = gcCode[recyclerClickPos-1];

    }

    private void process(View view){
        //Set image and name
        gcDP.setImageBitmap(Singleton.getInstance().getCurrentGCdp());
        gcName.setText(Singleton.getInstance().getCurrentGCname());

        //Add GC Message List
        addFrag();

        getLastIndex();

        //Onclick listeners
        backButGC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.popBackStack();
            }
        });

        sendButGC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chatMembers = Singleton.getInstance().getChatMembersList();
                totalMsgs = Singleton.getInstance().getGcMsgSize();
                chatCode = "msg_"+currUid+"_"+totalMsgs;
                groupChatMsg = messageBoxGC.getText().toString();
                Log.e(TAG, "TotalMsgs: " + totalMsgs);


                //Send message to all members
                for (int i=0; i<chatMembers.size();i++) {
                    groupRef.child(chatMembers.get(i)).child(currGcCode).child("chatmsg").child(chatCode).setValue(groupChatMsg);
                }

                messageBoxGC.setText("");
            }
        });
    }

    public void getLastIndex(){
        Query query = groupRef.child(currUid).child(currGcCode).child("chatmsg").orderByKey().limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snap: dataSnapshot.getChildren()){
                    lastIndexKey = snap.getKey();
                    Log.e(TAG, "Last Index: " + lastIndexKey);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void addFrag(){
        ChatFragGroupMsgList gcMsgList = new ChatFragGroupMsgList();
        manager = Singleton.getInstance().getManager();
        transaction = manager.beginTransaction();
        transaction.add(R.id.scrollChatViewGC, gcMsgList, "Messages");
        transaction.commit();
        Log.d(TAG, "addFrag: in "+TAG);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, TAG+ " onStop");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");

    }


}
