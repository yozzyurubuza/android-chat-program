package com.example.xtrimitea.fragmenttransactions.SlidingTab_JsonRecyclerView;

class JsonData {

    private String txt;
    private String img;


    public JsonData(String txt) {
        this.txt = txt;
        //this.img = img;
    }

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

}
