package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;

import static android.app.Activity.RESULT_OK;
import static android.content.ClipDescription.MIMETYPE_TEXT_PLAIN;

public class ChatFragUploadImage extends Fragment {
    private static final String TAG = ChatFragUploadImage.class.getSimpleName();

    private ImageView upImgPreview;
    private EditText upImageURL;
    private Button upImgBut, updateImgBut, pasteBut, clearBut, selectImgBut;

    private String getImgURL, currUid;
    private Boolean foundData = false;

    private DatabaseReference userRef;
    private ProgressDialog progressDialog;
    private ProgressDialog progressDialog2;

    private static final int PICK_IMAGE_REQUEST = 1;
    private Uri imageuri;
    private String fireURL;

    private StorageReference mStorageRef;
    private DatabaseReference mDatabaseRef;

    ProgressDialog uploadProgress;


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_frag_upimg, container, false);
        Log.d(TAG, TAG+ " onCreateView");

        init(view);
        process(view);

        return view;
    }

    private void init(View view){
        upImageURL = view.findViewById(R.id.upImageURL);
        upImgBut = view.findViewById(R.id.upImgBut);
        updateImgBut = view.findViewById(R.id.updateImgBut);
        upImgPreview = view.findViewById(R.id.upImagePreview);
        pasteBut = view.findViewById(R.id.pasteBut);
        clearBut = view.findViewById(R.id.clearBut);
        selectImgBut = view.findViewById(R.id.selectImgBut);

        currUid = Singleton.getInstance().getCurrUid();

        userRef = FirebaseDatabase.getInstance().getReference().child("users");
        mStorageRef = FirebaseStorage.getInstance().getReference("uploads");
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("uploads");

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Updating image...");

        progressDialog2 = new ProgressDialog(getActivity());
        progressDialog2.setMessage("Fetching image...");

        uploadProgress = new ProgressDialog(getActivity());
        uploadProgress.setMessage("Uploading image....");

    }

    private void process(View view){

        //Paste button function
        pasteBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Call paste function
                paste();
            }
        });

        //Clear Button function
        clearBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upImageURL.setText("");
            }
        });



        //Get image url inserted
        upImgBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getImgURL = upImageURL.getText().toString();
                progressDialog2.show();
                new Singleton.DownloadImageTask(upImgPreview)
                            .execute(getImgURL);
                delayFetch();
            }
        });

        //Upload image from gallery
        selectImgBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileChooser();
            }
        });


        //Save uploaded image url to database
        updateImgBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //progressDialog.show();

                uploadFile();
                //delayUpload();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null){

            imageuri = data.getData();
            convertUriToBitmap(imageuri);
            updateImgBut.setVisibility(View.VISIBLE);

        }

    }

    public void openFileChooser(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);

    }

    public void convertUriToBitmap(Uri imageuri){
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageuri);
            upImgPreview.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getFileExtension (Uri uri){
        ContentResolver cR = getActivity().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();

        return mime.getExtensionFromMimeType(cR.getType(uri));
    }


    public void uploadFile(){
        if (imageuri != null){
            final StorageReference fileReference = mStorageRef.child(System.currentTimeMillis()
            +"."+getFileExtension(imageuri));
            UploadTask uploadTask = fileReference.putFile(imageuri);
            uploadProgress.show();
            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()){
                        throw task.getException();
                    }
                    return fileReference.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()){
                        Uri taskResult = task.getResult();
                        fireURL = taskResult.toString();
                        Log.e(TAG, taskResult.toString());
                        UploadToFirebase upload = new UploadToFirebase(currUid, fireURL);
                        mDatabaseRef.child("uploadID").setValue(upload);
                        userRef.child(currUid).child("imageurl").setValue(fireURL);
                        Singleton.getInstance().setUserimgurl(fireURL);
                        Singleton.getInstance().setDownloadUserImage(false);
                        Toast.makeText(getActivity(), "Upload Success!", Toast.LENGTH_SHORT).show();
                        uploadProgress.dismiss();
                    }
                }
            });

//            fileReference.putFile(imageuri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                @Override
//                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
//
//                }
//            }).addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception e) {
//                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
//                }
//            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
//                @Override
//                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
//                    double progress = (100.0* taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
//                    uploadProgress.setProgress((int) progress);
//                    uploadProgress.show();
//
//                }
//            });

        } else {
            Toast.makeText(getActivity(), "No image selected", Toast.LENGTH_SHORT).show();
        }

    }

    public void paste(){
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        String pasteData = "";

        // If it does contain data, decide if you can handle the data.
        if (!(clipboard.hasPrimaryClip())) {

        } else if (!(clipboard.getPrimaryClipDescription().hasMimeType(MIMETYPE_TEXT_PLAIN))) {

            // since the clipboard has data but it is not plain text
            Log.e(TAG, "Data is not plain text...");

        } else {

            //since the clipboard contains plain text.
            ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);

            // Gets the clipboard as text.
            pasteData = item.getText().toString();
            upImageURL.setText(pasteData);
        }

    }

    public void delayUpload(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Image updated!", Toast.LENGTH_SHORT).show();
            }
        }, 3000);
    }

    public void delayFetch(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                //Check if insert url is possible or not
                if (Singleton.getInstance().getInvalidImgURL()){
                    Toast.makeText(getActivity(), "Invalid Image URL, please try a different one.", Toast.LENGTH_SHORT).show();
                    Singleton.getInstance().setInvalidImgURL(false);
                }
                progressDialog2.dismiss();

            }
        }, 2000);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        progressDialog.dismiss();
        progressDialog2.dismiss();
        Log.d(TAG, TAG+ " onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");

    }


}
