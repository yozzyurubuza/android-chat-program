package com.example.xtrimitea.fragmenttransactions.ChatApp;


import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Singleton {
    private static Singleton sInstance = new Singleton();

    public static Singleton getInstance(){
        if (sInstance == null){
            sInstance = new Singleton();
        }
        return sInstance;
    }


    //Variables
    private static final String TAG = Singleton.class.getSimpleName();
    private String backStackList;
    private int numOfRegUsers;
    private String progressReg;
    private String currEmail, currUserID;
    private String lname, fname, contactnum, userimgurl;
    private String friends;

    private int recyclerClickPos;
    private String[] chatFriendFullName;
    private String[] friendLists;
    private String[] userFriendList = new String[1000];
    private String[] friendListImages;
    private String friendNumDB;
    private String[] friendsUid = new String[1000];
    private String[] newFriendImgURL = new String[1000];
    private String[] newFriendFullname = new String[1000];
    private String currUid, chatUid;
    private int numOfFriendsAdded;
    private int listAdapterSize = 0, gcListAdapterSize = 0;
    private int gcPos = 0, gcListAdapterPos = 0;
    private List<JsonData> storedList;
    private List<String> addToGC = new ArrayList<String>();
    private String[] gcName = new String[1000];
    private String[] gcImage = new String[1000];
    private String currentGCname;
    private Bitmap currentGCdp;
    private Bitmap[] gcImageList = new Bitmap[1000];
    private List<JsonData> itemsGC = new ArrayList<JsonData>();
    private List<String> gcImageArray = new ArrayList<String>();

    private List<String> chatMembersList = new ArrayList<String>();
    private List<String> messageList = new ArrayList<String>();
    private String currentGcCode;
    private String[] gcCode = new String[1000];
    private int gcMsgSize = 0;
    private String[] gcCreator = new String[1000];
    private int grpListAdapterPos = 0;

    private List<String> friendsAdded = new ArrayList<String>();

    //Objects
    private FragmentManager manager;
    private ProgressDialog progressDialog;
    private DatabaseReference mDatabaseUserFriends;
    private DatabaseReference mDatabaseUsers;
    private boolean valEmail;
    private Boolean downloadImageFriends, downloadUserImage, downloadGCImage = false;
    private Boolean invalidImgURL = false;
    private Boolean isInAddContact = false;
    private Boolean isInCreateGC = false;
    private Boolean[] isClickedForGC = new Boolean[1000];


    private int numOfFriends;
    private int imageListFriendsPos;
    private int listAdapterPos = 0;
    private Bitmap[] imageListFriends;
    private Bitmap imageUser;
    private Bitmap[] bitmapStore = new Bitmap[1000];
    private Bitmap currentChatImage;
    private int groupChatFlist = 0;
    private List<String> groupChatList = new ArrayList<String>();


    //Variables get and set


    public List<String> getGroupChatList() {
        return groupChatList;
    }

    public void setGroupChatList(List<String> groupChatList) {
        this.groupChatList = groupChatList;
    }

    public String getBackStackList() {
        return backStackList;
    }

    public void setBackStackList(String backStackList) {
        this.backStackList = backStackList;
    }

    public int getNumOfRegUsers() {
        return numOfRegUsers;
    }

    public void setNumOfRegUsers(int numOfRegUsers) {
        this.numOfRegUsers = numOfRegUsers;
    }

    public String getProgressReg() {
        return progressReg;
    }

    public void setProgressReg(String progressReg) {
        this.progressReg = progressReg;
    }

    public String getCurrEmail() {
        return currEmail;
    }

    public void setCurrEmail(String currEmail) {
        this.currEmail = currEmail;
    }

    public String getCurrUserID() {
        return currUserID;
    }

    public void setCurrUserID(String currUserID) {
        this.currUserID = currUserID;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getContactnum() {
        return contactnum;
    }

    public void setContactnum(String contactnum) {
        this.contactnum = contactnum;
    }

    public String getFriends() {
        return friends;
    }

    public void setFriends(String friends) {
        this.friends = friends;
    }

    public String getUserimgurl() {
        return userimgurl;
    }

    public void setUserimgurl(String userimgurl) {
        this.userimgurl = userimgurl;
    }

    public String getChatFriendFullName(int i) {
        return chatFriendFullName[i];
    }

    public void setChatFriendFullName(String[] chatFriendFullName) {
        this.chatFriendFullName = chatFriendFullName;
    }

    public String getFriendLists(int i) {
        return friendLists[i];
    }

    public void setFriendLists(String[] friendLists) {
        this.friendLists = friendLists;
    }

    public int getRecyclerClickPos() {
        return recyclerClickPos;
    }

    public void setRecyclerClickPos(int recyclerClickPos) {
        this.recyclerClickPos = recyclerClickPos;
    }

    public String getFriendListImages(int i) {
        return friendListImages[i];
    }

    public void setFriendListImages(String[] friendListImages) {
        this.friendListImages = friendListImages;
    }

    public int getImageListFriendsPos() {
        return imageListFriendsPos;
    }

    public void setImageListFriendsPos(int imageListFriendsPos) {
        this.imageListFriendsPos = imageListFriendsPos;
    }

    public int getNumOfFriends() {
        return numOfFriends;
    }

    public void setNumOfFriends(int numOfFriends) {
        this.numOfFriends = numOfFriends;
    }

    public int getListAdapterPos() {
        return listAdapterPos;
    }

    public void setListAdapterPos(int listAdapterPos) {
        this.listAdapterPos = listAdapterPos;
    }

    public String getFriendNumDB() {
        return friendNumDB;
    }

    public void setFriendNumDB(String friendNumDB) {
        this.friendNumDB = friendNumDB;
    }

    public String getCurrUid() {
        return currUid;
    }

    public void setCurrUid(String currUid) {
        this.currUid = currUid;
    }

    public String getChatUid() {
        return chatUid;
    }

    public void setChatUid(String chatUid) {
        this.chatUid = chatUid;
    }

    public String[] getFriendsUid() {
        return friendsUid;
    }

    public void setFriendsUid(String[] friendsUid) {
        this.friendsUid = friendsUid;
    }

    public String[] getUserFriendList() {
        return userFriendList;
    }

    public void setUserFriendList(String[] userFriendList) {
        this.userFriendList = userFriendList;
    }

    public int getNumOfFriendsAdded() {
        return numOfFriendsAdded;
    }

    public void setNumOfFriendsAdded(int numOfFriendsAdded) {
        this.numOfFriendsAdded = numOfFriendsAdded;
    }

    public Boolean getInvalidImgURL() {
        return invalidImgURL;
    }

    public void setInvalidImgURL(Boolean invalidImgURL) {
        this.invalidImgURL = invalidImgURL;
    }

    public int getListAdapterSize() {
        return listAdapterSize;
    }

    public void setListAdapterSize(int listAdapterSize) {
        this.listAdapterSize = listAdapterSize;
    }

    public String[] getNewFriendImgURL() {
        return newFriendImgURL;
    }

    public void setNewFriendImgURL(String[] newFriendImgURL) {
        this.newFriendImgURL = newFriendImgURL;
    }

    public Boolean getInAddContact() {
        return isInAddContact;
    }

    public void setInAddContact(Boolean inAddContact) {
        isInAddContact = inAddContact;
    }

    public List<JsonData> getStoredList() {
        return storedList;
    }

    public void setStoredList(List<JsonData> storedList) {
        this.storedList = storedList;
    }

    public Boolean getInCreateGC() {
        return isInCreateGC;
    }

    public void setInCreateGC(Boolean inCreateGC) {
        isInCreateGC = inCreateGC;
    }

    public Boolean[] getIsClickedForGC() {
        return isClickedForGC;
    }

    public void setIsClickedForGC(Boolean[] isClickedForGC) {
        this.isClickedForGC = isClickedForGC;
    }

    public String[] getNewFriendFullname() {
        return newFriendFullname;
    }

    public void setNewFriendFullname(String[] newFriendFullname) {
        this.newFriendFullname = newFriendFullname;
    }

    public List<String> getAddToGC() {
        return addToGC;
    }

    public void setAddToGC(List<String> addToGC) {
        this.addToGC = addToGC;
    }

    public Boolean getDownloadGCImage() {
        return downloadGCImage;
    }

    public void setDownloadGCImage(Boolean downloadGCImage) {
        this.downloadGCImage = downloadGCImage;
    }

    public String[] getGcName() {
        return gcName;
    }

    public void setGcName(String[] gcName) {
        this.gcName = gcName;
    }

    public String[] getGcImage() {
        return gcImage;
    }

    public void setGcImage(String[] gcImage) {
        this.gcImage = gcImage;
    }

    public String getCurrentGCname() {
        return currentGCname;
    }

    public void setCurrentGCname(String currentGCname) {
        this.currentGCname = currentGCname;
    }

    public int getGcPos() {
        return gcPos;
    }

    public void setGcPos(int gcPos) {
        this.gcPos = gcPos;
    }

    public int getGcListAdapterPos() {
        return gcListAdapterPos;
    }

    public void setGcListAdapterPos(int gcListAdapterPos) {
        this.gcListAdapterPos = gcListAdapterPos;
    }


    public List<String> getGcImageArray() {
        return gcImageArray;
    }

    public void setGcImageArray(List<String> gcImageArray) {
        this.gcImageArray = gcImageArray;
    }

    public List<String> getChatMembersList() {
        return chatMembersList;
    }

    public void setChatMembersList(List<String> chatMembersList) {
        this.chatMembersList = chatMembersList;
    }

    public List<String> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<String> messageList) {
        this.messageList = messageList;
    }

    public String getCurrentGcCode() {
        return currentGcCode;
    }

    public void setCurrentGcCode(String currentGcCode) {
        this.currentGcCode = currentGcCode;
    }

    public String[] getGcCode() {
        return gcCode;
    }

    public void setGcCode(String[] gcCode) {
        this.gcCode = gcCode;
    }

    public int getGcMsgSize() {
        return gcMsgSize;
    }

    public void setGcMsgSize(int gcMsgSize) {
        this.gcMsgSize = gcMsgSize;
    }

    public String[] getGcCreator() {
        return gcCreator;
    }

    public void setGcCreator(String[] gcCreator) {
        this.gcCreator = gcCreator;
    }

    public int getGrpListAdapterPos() {
        return grpListAdapterPos;
    }

    public void setGrpListAdapterPos(int grpListAdapterPos) {
        this.grpListAdapterPos = grpListAdapterPos;
    }

    public List<String> getFriendsAdded() {
        return friendsAdded;
    }

    public void setFriendsAdded(List<String> friendsAdded) {
        this.friendsAdded = friendsAdded;
    }

    public int getGroupChatFlist() {
        return groupChatFlist;
    }

    public void setGroupChatFlist(int groupChatFlist) {
        this.groupChatFlist = groupChatFlist;
    }

    ////////////////////////
    //Objects get set

    public FragmentManager getManager() {
        return manager;
    }

    public void setManager(FragmentManager manager) {
        this.manager = manager;
    }

    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    public DatabaseReference getmDatabaseUserFriends() {
        return mDatabaseUserFriends;
    }

    public void setmDatabaseUserFriends(DatabaseReference mDatabaseUserFriends) {
        this.mDatabaseUserFriends = mDatabaseUserFriends;
    }

    public DatabaseReference getmDatabaseUsers() {
        return mDatabaseUsers;
    }

    public DatabaseReference setmDatabaseUsers(DatabaseReference mDatabaseUsers) {
        this.mDatabaseUsers = mDatabaseUsers;
        return mDatabaseUsers;
    }

    public boolean getValEmail() {
        return valEmail;
    }

    public void setValEmail(boolean valEmail) {
        this.valEmail = valEmail;
    }

    public Bitmap getImageListFriends(int pos) {
        return imageListFriends[pos];
    }

    public void setImageListFriends(Bitmap[] imageFriends) {
        this.imageListFriends = imageFriends;
    }

    public Boolean getDownloadImageFriends() {
        return downloadImageFriends;
    }

    public void setDownloadImageFriends(Boolean downloadImageFriends) {
        this.downloadImageFriends = downloadImageFriends;
    }

    public Boolean getDownloadUserImage() {
        return downloadUserImage;
    }

    public void setDownloadUserImage(Boolean downloadUserImage) {
        this.downloadUserImage = downloadUserImage;
    }

    public Bitmap getImageUser() {
        return imageUser;
    }

    public void setImageUser(Bitmap imageUser) {
        this.imageUser = imageUser;
    }

    public Bitmap[] getBitmapStore() {
        return bitmapStore;
    }

    public void setBitmapStore(Bitmap[] bitmapStore) {
        this.bitmapStore = bitmapStore;
    }

    public Bitmap getCurrentGCdp() {
        return currentGCdp;
    }

    public void setCurrentGCdp(Bitmap currentGCdp) {
        this.currentGCdp = currentGCdp;
    }

    public int getGcListAdapterSize() {
        return gcListAdapterSize;
    }

    public void setGcListAdapterSize(int gcListAdapterSize) {
        this.gcListAdapterSize = gcListAdapterSize;
    }

    public Bitmap[] getGcImageList() {
        return gcImageList;
    }

    public void setGcImageList(Bitmap[] gcImageList) {
        this.gcImageList = gcImageList;
    }

    public List<JsonData> getItemsGC() {
        return itemsGC;
    }

    public void setItemsGC(List<JsonData> itemsGC) {
        this.itemsGC = itemsGC;
    }


    //public functions
    public void getDetailsFromDB(){
        //Declare needed objects for getting DB and changing Fragments
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("users");
                Singleton.getInstance().setmDatabaseUsers(mDatabaseUsers);
        final DatabaseReference mDatabaseUserFriends = FirebaseDatabase.getInstance().getReference().child("userfriends");
                Singleton.getInstance().setmDatabaseUserFriends(mDatabaseUserFriends);


        currEmail = Singleton.getInstance().getCurrEmail();

        final Boolean[] foundData = {false, false};
        final FragmentManager[] manager = new FragmentManager[1];
        final FragmentTransaction[] transaction = new FragmentTransaction[1];
        progressDialog = Singleton.getInstance().getProgressDialog();

        //Start retrieving data fromg DB by matching the current email used in logging in
        Log.e("TAG", "The current email is: "+currEmail);
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot val : dataSnapshot.getChildren()){
                    //Search for a value inside child (child=email;value=qwe1@yahoo.com)
                    if (foundData[0] == false) {
                        //Display all child value (uid)
                        Log.e(TAG, String.valueOf(val));
                        //Get details found using current email
                        if (val.child("email").getValue(String.class).contains(currEmail)) {
                            Singleton.getInstance().setCurrUid(val.getKey());
                            Log.e(TAG, "Current uid: "+val.getKey());

                            //Get Last Name
                            Singleton.getInstance().setLname(val.child("lname").getValue(String.class));
                            Log.e(TAG, val.child("lname").getValue(String.class));
                            //Get First Name
                            Singleton.getInstance().setFname(val.child("fname").getValue(String.class));
                            Log.e(TAG, val.child("fname").getValue(String.class));

                            //Get Contact Number
                            Singleton.getInstance().setContactnum(val.child("contactnum").getValue(String.class));
                            Log.e(TAG, val.child("contactnum").getValue(String.class));

                            //Get Image URL
                            Singleton.getInstance().setUserimgurl(val.child("imageurl").getValue(String.class));
                            Log.e(TAG,Singleton.getInstance().getUserimgurl());

                            //Stop displaying data if found
                            foundData[0] = true;

                            //Fragment Transactions to change login to home
                            ChatFragHome home = new ChatFragHome();
                            manager[0] = Singleton.getInstance().getManager();
                            transaction[0] = manager[0].beginTransaction();
                            transaction[0].replace(R.id.mainLayout, home, "Home");
                            manager[0].popBackStack();
                            transaction[0].commit();
                            progressDialog.hide();

                        } else
                            Log.e("TAG", "Searching....");
                    }
                }
                if (foundData[0] == false){
                    //No data was found using the current email after searching the whole database
                    Log.e("TAG", "Searching details failed!");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        //Get friend list
        mDatabaseUserFriends.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot val : dataSnapshot.getChildren()) {
                    //Get all email of friends of the current logged-in email
                    Log.e(TAG, val + " 2");

                    if (foundData[1] == false) {
                        if (val.child("email").getValue(String.class).contains(currEmail)) {
                            for (int j = 0; j < val.child("friendlist").getChildrenCount(); j++) {
                                Singleton.getInstance().setFriends(val.child("friendlist").child("friend" + String.valueOf(j + 1)).getValue(String.class));
                                Log.e(TAG, Singleton.getInstance().getFriends() + "");
                            }
                            foundData[1] = true;
                        } else
                            Log.e(TAG, "Search 3 failed!");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //Get num of friends
        mDatabaseUserFriends.child("friendlist").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Singleton.getInstance().setNumOfFriends((int) dataSnapshot.getChildrenCount());
                Log.e(TAG, "numOfFriends in GetFriendsFromDB: "+Singleton.getInstance().getNumOfFriends());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }



    //Process image from URL
    public static class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }


        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                Singleton.getInstance().setInvalidImgURL(false);
                InputStream in = new java.net.URL(urldisplay).openStream();
                Log.e(TAG, "Currently parsing image: "+ urldisplay);
                mIcon11 = BitmapFactory.decodeStream(in);
                Log.e(TAG, "Parsing success: " +mIcon11);
            } catch (Exception e) {
                Singleton.getInstance().setInvalidImgURL(true);
                Log.e("Error", e.getMessage());
                e.printStackTrace();
                //If no imageurl was found, use default no image file from net
                try {
                    InputStream in = new java.net.URL("http://lequytong.com/Content/Images/no-image-02.png").openStream();
                    mIcon11 = BitmapFactory.decodeStream(in);
                    return mIcon11;
                }catch (Exception e1){
                    Log.e(TAG, "Default Image URL not found!");
                }

            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);

            //New
            int pos, gcPos, numOfFriends;
            pos = Singleton.getInstance().getImageListFriendsPos();
            gcPos = Singleton.getInstance().getGcPos();

            //Save user image in a singleton so no more downloading later
            if (Singleton.getInstance().getDownloadUserImage() == false){
                if(Singleton.getInstance().getInvalidImgURL()==false) {
                    Singleton.getInstance().setImageUser(result);
                    Singleton.getInstance().setDownloadUserImage(true);
                }
            }

            //if images boolean checker is false and if downloaded images is less than number of friends
            if (!Singleton.getInstance().getDownloadImageFriends() && !Singleton.getInstance().getInAddContact()){
                if (Singleton.getInstance().getImageListFriendsPos() <= Singleton.getInstance().getNumOfFriends()+2) {
                    Bitmap[] bitmaps = Singleton.getInstance().getBitmapStore();
                    bitmaps[pos] = result;
                    Log.e(TAG, "Bitmaps[pos]: "+bitmaps[pos]);
                    Log.e(TAG, "Bitmaps Array: "+ Arrays.toString(bitmaps));

                    Singleton.getInstance().setImageListFriends(bitmaps);
                    Log.e(TAG, "ImaageListFriends: "+Singleton.getInstance().getImageListFriends(pos)+"");

                    pos++;
                    Singleton.getInstance().setImageListFriendsPos(pos);
                    Log.e(TAG, "ImageListFriendsPos: "+pos);
                    numOfFriends = Singleton.getInstance().getNumOfFriends();
                    Log.e(TAG, "getNumOfFriends: "+ String.valueOf(Singleton.getInstance().getNumOfFriends()+2));

                    if (Singleton.getInstance().getImageListFriendsPos() == Singleton.getInstance().getNumOfFriends()+2 ){
                        Log.e(TAG, "setDownloadImageFriends: Done!");
                        Log.e(TAG, "numOfFriends: "+Singleton.getInstance().getNumOfFriends());
                        Singleton.getInstance().setDownloadImageFriends(true);
                    }
                }
            }

            //Saving converted GC images into array
            else if (!Singleton.getInstance().getDownloadGCImage() && Singleton.getInstance().getDownloadImageFriends()){
                Bitmap[] bitmaps = Singleton.getInstance().getGcImageList();
                bitmaps[gcPos] = result;
                Log.e(TAG, "GC Bitmaps[pos]: "+bitmaps[gcPos]);
                Log.e(TAG, "GC Bitmaps Array: "+Arrays.toString(bitmaps));

                Singleton.getInstance().setGcImageList(bitmaps);
                gcPos++;
                Singleton.getInstance().setGcPos(gcPos);

                if(Singleton.getInstance().getGcPos() == Singleton.getInstance().getItemsGC().size()){
                    Log.e(TAG, "GC items size: "+Singleton.getInstance().getItemsGC().size());
                    Log.e(TAG, "setDownloadGCImage: Done! ");
                    Singleton.getInstance().setDownloadGCImage(true);
                }

            }



            //New

        }
    }

    //Return Data to defaults
    public void clearData(){
        Log.e(TAG, "Clearing Singleton arrays and data...");
        //Clear Friends UID
        Singleton.getInstance().setFriendsUid(new String[1000]);
        //Clear full names
        Singleton.getInstance().setChatFriendFullName(new String[1000]);
        //Clear friend email
        Singleton.getInstance().setFriendLists(new String[1000]);
        Singleton.getInstance().setUserFriendList(new String[1000]);
        //Clear friend list images
        Singleton.getInstance().setFriendListImages(new String[1000]);
        Singleton.getInstance().setBitmapStore(new Bitmap[1000]);

        //Clear downloadUserImage and downloadImageFriends
        Singleton.getInstance().setDownloadUserImage(false);
        Singleton.getInstance().setDownloadImageFriends(false);

        //Clear GC stuff
        Singleton.getInstance().setDownloadGCImage(false);
        Singleton.getInstance().setGcPos(0);
        Singleton.getInstance().setItemsGC(new ArrayList<JsonData>());
        Singleton.getInstance().setGcImageList(new Bitmap[1000]);

    }

    //Add delay timer
    public void delay(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                progressDialog.dismiss();
            }
        }, 3000);
    }


    //Sample code to get key / child
    public void getDetailsFromDB2(){
        final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("users");
        currEmail = Singleton.getInstance().getCurrEmail();
        final String[] getParent = new String[1];
        Log.e("TAG", "The current email is: "+currEmail);
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot val : dataSnapshot.getChildren()){
                    //Display all child value (uid)
                    Log.e("TAG", String.valueOf(val));
                    //Search for Key
                    if (val.getKey().contains("uid")){
                        getParent[0] = String.valueOf(mDatabase.getParent());
                        Log.e("TAG", getParent[0]);
                    }
                    else
                        Log.e("TAG", "Search failed!");

                    //Search for a value inside child (child=email;value=qwe1@yahoo.com)
                    if (val.child("email").getValue(String.class).contains(currEmail)){
                        Log.e("TAG", val.child("fname").getValue(String.class));
                    }
                    else
                        Log.e("TAG", "Search 2 failed!");

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
