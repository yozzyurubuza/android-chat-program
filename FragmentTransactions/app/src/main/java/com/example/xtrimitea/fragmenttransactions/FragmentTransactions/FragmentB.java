package com.example.xtrimitea.fragmenttransactions.FragmentTransactions;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xtrimitea.fragmenttransactions.R;

//2. Duplicate Fragment B and layout for Fragment B
public class FragmentB extends Fragment {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("TAG","Fragment B onAttach");
    }

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("TAG","Fragment B onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bb, container, false);
        Log.d("TAG","Fragment B onCreateView");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("TAG","Fragment B onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TAG","Fragment B onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TAG","Fragment B onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("TAG","Fragment B onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TAG","Fragment B onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("TAG","Fragment B onDetach");
    }

}
