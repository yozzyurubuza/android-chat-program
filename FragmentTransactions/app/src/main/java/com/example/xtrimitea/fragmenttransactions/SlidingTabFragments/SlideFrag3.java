package com.example.xtrimitea.fragmenttransactions.SlidingTabFragments;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xtrimitea.fragmenttransactions.R;

public class SlideFrag3 extends Fragment {
    public static int mFragNum = 0;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("TAG","SlideFrag3 onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("TAG","SlideFrag3 onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_c, container, false);

        //Second tabLayout
        tabLayout = (TabLayout) view.findViewById(R.id.tabLayoutC);
        tabLayout.addTab(tabLayout.newTab().setText("Tab 1"));
        tabLayout.addTab(tabLayout.newTab().setText("Tab 2"));
        tabLayout.addTab(tabLayout.newTab().setText("Tab 3"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) view.findViewById(R.id.viewPagerC);
        pagerAdapter = new PagerAdapter(getChildFragmentManager(), tabLayout.getTabCount(),1);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        Log.e("TAG", "ViewPager_C initialized");

        Log.d("TAG","SlideFrag3 onCreateView");
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("TAG","SlideFrag3 onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TAG","SlideFrag3 onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TAG","SlideFrag3 onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("TAG","SlideFrag3 onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TAG","SlideFrag3  onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("TAG","SlideFrag3 onDetach");
    }

}
