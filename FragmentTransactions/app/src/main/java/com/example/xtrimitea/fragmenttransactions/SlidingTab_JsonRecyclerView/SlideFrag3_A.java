package com.example.xtrimitea.fragmenttransactions.SlidingTab_JsonRecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xtrimitea.fragmenttransactions.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SlideFrag3_A extends Fragment {

    //RecyclerView Codes
    private static final String TAG = SlideFrag3_A.class.getSimpleName();
    private List<JsonData> items;
    private RecyclerView recyclerView;
    private ListAdapter listAdapter;
    private JSONArray jsonArray, jsonArray2;
    private JsonData jdata;
    private String carBrand;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG,TAG+" onAttach");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.d(TAG, "setUserVisibleHint in " + TAG);
            Singleton.getInstance().setFragChilPos(3);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG,TAG+" onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view2;
        view2 = inflater.inflate(R.layout.stjrv_fragment_vlist, container, false);

        init(view2);
        process();

        Log.d(TAG,TAG+" onCreateView");
        return view2;
    }

    private void init(View view){
        //RecyclerView Code
        jsonArray = Singleton.getInstance().getJsonArrayBot();
        carBrand = Singleton.getInstance().getCarBrand3();
        items = new ArrayList<JsonData>();

        recyclerView = view.findViewById(R.id.recyclerViewCarBrand);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
    }


    private void process() {
        //Set Lower RecyclerView values based on the MainFragPos
        try {
            if(Singleton.getInstance().getMainFragPos() == 1) {
                jsonArray2 = jsonArray.getJSONObject(0).getJSONArray("CarBrands")
                        .getJSONObject(2).getJSONArray(carBrand +"Sedan");
            }

            else if(Singleton.getInstance().getMainFragPos() == 2) {
                jsonArray2 = jsonArray.getJSONObject(1).getJSONArray("CarBrands")
                        .getJSONObject(2).getJSONArray(carBrand +"SC");
            }

            else if(Singleton.getInstance().getMainFragPos() == 3) {
                jsonArray2 = jsonArray.getJSONObject(2).getJSONArray("CarBrands")
                        .getJSONObject(2).getJSONArray(carBrand +"SUV");
            }

            for(int i=0; i<jsonArray2.length(); i++){
                jdata = Singleton.getInstance().setJdata(
                        new JsonData(jsonArray2.getJSONObject(i).getString("VehicleList")));
                items.add(Singleton.getInstance().getJdata());
            }
            listAdapter = new ListAdapter(getChildFragmentManager(),items);
            recyclerView.setAdapter(listAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG,TAG+" onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG,TAG+" onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG,TAG+" onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG,TAG+" onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG,TAG+" onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG,TAG+" onDetach");
    }
}
