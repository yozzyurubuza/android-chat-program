package com.example.xtrimitea.fragmenttransactions.SlidingTabFragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.xtrimitea.fragmenttransactions.R;

public class MainActivity2 extends AppCompatActivity implements FragmentManager.OnBackStackChangedListener {

//1. Android Codes
//1.1 Create FragmentManager and FragmentTransaction

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction;

        //1.1.1 Declare frag1 to be displayed in displayfrag1
        Act2_Frag1 frag1 = new Act2_Frag1();
        transaction = manager.beginTransaction();
        transaction.add(R.id.displayfrag1, frag1, "A");
        transaction.commit();


    }


    @Override
    public void onBackStackChanged() {

    }
}
