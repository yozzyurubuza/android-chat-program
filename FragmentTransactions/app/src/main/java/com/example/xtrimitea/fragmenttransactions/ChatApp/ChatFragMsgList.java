package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChatFragMsgList extends Fragment {
    private static final String TAG = ChatFragMsgList.class.getSimpleName();

    private ChatMessages chatMessages;
    private List<ChatMessages> messagesList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MsgListAdapter msgListAdapter;
    private RecyclerView recyclerMsg;

    private DatabaseReference chatRef;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_recycler_flist, container,false);
        Log.d(TAG, TAG+ " onCreateView");

        init(view);
        process(view);


        return view;
    }


    private void init(View view) {
        chatRef = FirebaseDatabase.getInstance().getReference().child("userfriends");

        recyclerMsg = view.findViewById(R.id.recyclerFriends);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setStackFromEnd(true);
        recyclerMsg.setLayoutManager(linearLayoutManager);

    }

    private void process(View view) {

        final Boolean[] foundData = {false};

        //Parsing message data depending on who was clicked and retrieving the messages

        chatRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                messagesList.clear();
                for(DataSnapshot snap: dataSnapshot.getChildren()){
                    if (snap.child("email").getValue().equals(Singleton.getInstance().getCurrEmail())) {
                        snap = snap.child("chatmsg").child(Singleton.getInstance().getChatUid());
                        for(int i=0; i<=snap.getChildrenCount();i++) {
                            int j = 1, k = 2;
                            if (snap.child("msg"+i+"_"+j).getValue() != null){
                                String sender = snap.child("msg"+i+"_"+j).getValue(String.class);
                                String receiver = "null";
                                Log.e(TAG, "Sender message found! "+sender);
                                chatMessages = new ChatMessages(sender, sender, receiver);
                                messagesList.add(chatMessages);
                            }
                            else if (snap.child("msg"+i+"_"+k).getValue() != null){
                                String sender = "null";
                                String receiver = snap.child("msg"+i+"_"+k).getValue(String.class);
                                Log.e(TAG, "Receiver message found! "+receiver);
                                chatMessages = new ChatMessages(receiver, sender, receiver);
                                messagesList.add(chatMessages);
                            }
                        }
                        foundData[0] = true;
                    }
                }
                msgListAdapter = new MsgListAdapter(getChildFragmentManager(), messagesList);
                recyclerMsg.setAdapter(msgListAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, TAG+ " onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");

    }


}
