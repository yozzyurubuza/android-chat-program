package com.example.xtrimitea.fragmenttransactions.SlidingTab_JsonRecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xtrimitea.fragmenttransactions.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SlideFrag1 extends Fragment {

    private static final String TAG = SlideFrag1.class.getSimpleName();

    //RecyclerView Codes
    private List<JsonData> items;
    private RecyclerView recyclerView;
    private ListAdapter listAdapter;
    private JSONObject jsonObject;
    private JSONArray jsonArray, jsonArray2;
    private JsonData jdata;
    private FragmentTransaction transaction;
    private FragmentManager manager;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG,TAG+" onAttach");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.d(TAG, "setUserVisibleHint in " + TAG);
            Singleton.getInstance().setMainFragPos(1);
            addFrag();
        }
        else {
            removeFrag();
        }
    }

    public void addFrag(){
        Act2_Frag2 frag2 = new Act2_Frag2();
        manager = Singleton.getInstance().getFragmentManager();
        transaction = manager.beginTransaction();
        transaction.add(R.id.frameLayout, frag2, "lowerTab1");
        transaction.commit();
        Log.d(TAG, "addFrag: in "+TAG);
    }

    public void removeFrag(){
        Act2_Frag2 frag2 = new Act2_Frag2();
        manager = Singleton.getInstance().getFragmentManager();
        transaction = manager.beginTransaction();
        if(frag2 != null) {
            transaction.remove(frag2);
            transaction.commit();
            Log.d(TAG, "Executing remove frag2 in " + TAG);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG,TAG+" onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.stjrv_fragment_vlist, container, false);

        init(view);
        process();

        Log.d(TAG,TAG+" onCreateView");
        return view;
    }


    private void init(View view) {
        jsonArray = Singleton.getInstance().getJsonArray();
        recyclerView = view.findViewById(R.id.recyclerViewCarBrand);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
    }

    private void process() {

        Singleton.getInstance().setJsonArrayBot(Singleton.getInstance().getJsonArray());

        try {
            jsonArray = Singleton.getInstance().getJsonArray();
            jsonObject = jsonArray.getJSONObject(0);
            jsonArray2 = jsonObject.getJSONArray("CarBrands");

            Singleton.getInstance().setItems(new ArrayList<>());
            items = Singleton.getInstance().getItems();

            for (int i = 0; i < jsonArray2.length(); i++) {
                jdata = Singleton.getInstance().setJdata(
                        new JsonData(jsonArray2.getJSONObject(i).getString("CarBrand")));
                items.add(Singleton.getInstance().getJdata());
            }

            listAdapter = new ListAdapter(getChildFragmentManager(), items);
            recyclerView.setAdapter(listAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG,TAG+" onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG,TAG+" onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG,TAG+" onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG,TAG+" onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG,TAG+" onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG,TAG+" onDetach");
    }
}

//Old Tablayout and ViewPager Code
        /*try {

            //Second tabLayout
            tabLayout = view.findViewById(R.id.tabLayoutA);

            jsonObject = jsonArray.getJSONObject(0);
            jsonArray2 = jsonObject.getJSONArray("CarBrands");
            for (int i=0; i<jsonArray2.length();i++){
                Singleton.getInstance().setCarBrands(jsonArray.getJSONObject(0)
                        .getJSONArray("CarBrands")
                        .getJSONObject(i)
                        .getString("CarBrand"));
                tabLayout.addTab(tabLayout.newTab().setText(Singleton.getInstance().getCarBrands()));
            }

            tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

            viewPagerA = view.findViewById(R.id.viewPagerA);
            pagerAdapter = Singleton.getInstance().setPagerAdapter(new PagerAdapter(getChildFragmentManager(), tabLayout.getTabCount(), 1));
            viewPagerA.setAdapter(pagerAdapter);
            viewPagerA.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPagerA.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
            Log.e("TAG", "ViewPager initialized");

            //RecyclerView Code (Top)
            recyclerView = view.findViewById(R.id.recyclerViewCarBrand);
            Singleton.getInstance().setItems(new ArrayList<>());
            items = Singleton.getInstance().getItems();

            for (int i = 0; i < jsonArray2.length(); i++) {
                jdata = Singleton.getInstance().setJdata(
                        new JsonData(jsonArray2.getJSONObject(i).getString("CarBrand")));
                items.add(Singleton.getInstance().getJdata());
            }

            RecyclerView.LayoutManager layoutManager = Singleton.getInstance().setLayoutManager(new LinearLayoutManager(getActivity()));
            //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);

//            listAdapter = Singleton.getInstance().setListAdapter(new ListAdapter(getChildFragmentManager(), items));
            listAdapter = new ListAdapter(getChildFragmentManager(),items);
            recyclerView.setAdapter(listAdapter);

        }catch (JSONException e){
            e.printStackTrace();
        }*/

