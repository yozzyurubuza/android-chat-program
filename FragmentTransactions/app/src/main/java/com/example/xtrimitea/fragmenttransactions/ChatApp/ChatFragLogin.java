package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;

public class ChatFragLogin extends Fragment {

    private static final String TAG = ChatFragLogin.class.getSimpleName();
    private FragmentManager manager;
    private FragmentTransaction transaction;

    private Button signIn, magicSignIn, magicSignIn2;
    private EditText getEmail, getPassword;
    private View view;
    private TextView signUp;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, TAG+ " onCreateView");
        view = inflater.inflate(R.layout.chat_frag_login, container, false);

        init(view);
        process(view);

        return view;
    }

    public void init(View view){
        signIn = view.findViewById(R.id.signInBut);
        getEmail = view.findViewById(R.id.emailBox);
        getPassword = view.findViewById(R.id.passBox);
        signUp = view.findViewById(R.id.signUpTxt);
        magicSignIn = view.findViewById(R.id.magicSignIn);
        magicSignIn2 = view.findViewById(R.id.magicSignIn2);
        firebaseAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Signing In...");
        Singleton.getInstance().setProgressDialog(progressDialog);

        Singleton.getInstance().setDownloadUserImage(false);
        Singleton.getInstance().setDownloadImageFriends(false);
        Singleton.getInstance().setImageListFriendsPos(0);

    }


    public void process(View view){
        signIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getEmail.getText().toString().isEmpty() || getEmail.getText().toString().startsWith(" ")){
                    Toast.makeText(getActivity(),"Please insert email address",Toast.LENGTH_SHORT).show();
                    getEmail.requestFocus();
                }
                else if(getPassword.getText().toString().isEmpty() || getPassword.getText().toString().startsWith(" ")){
                    Toast.makeText(getActivity(),"Please insert password",Toast.LENGTH_SHORT).show();
                    getPassword.requestFocus();
                }
                else {
                    //Start login process with Firebase
                    progressDialog.show();
                    login();
                }

            }
        });

        signUp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ChatFragSignUp signUp = new ChatFragSignUp();
                manager = getActivity().getSupportFragmentManager();
                transaction = manager.beginTransaction();
                transaction.replace(R.id.mainLayout, signUp, "SignUp");
                transaction.addToBackStack("addSignUp");
                transaction.commit();
            }
        });

        magicSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getEmail.setText("qwe1@yahoo.com");
                getPassword.setText("qwe123");
                progressDialog.show();
                login();

            }
        });

        magicSignIn2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getEmail.setText("asd1@yahoo.com");
                getPassword.setText("asd123");
                progressDialog.show();
                login();
            }
        });

    }

    public void login(){
        firebaseAuth.signInWithEmailAndPassword(getEmail.getText().toString(),getPassword.getText().toString())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){

                            //Get details from DB upon logging in
                            Singleton.getInstance().setCurrEmail(getEmail.getText().toString());
                            Singleton.getInstance().getDetailsFromDB();
                        }
                        else if(!task.isSuccessful()) {
                            try {
                                throw task.getException();
                            }catch (FirebaseAuthInvalidUserException e){
                                Toast.makeText(getActivity(), "Invalid email address", Toast.LENGTH_SHORT).show();
                                getEmail.requestFocus();
                            }catch (FirebaseAuthInvalidCredentialsException e){
                                Toast.makeText(getActivity(), "Invalid password", Toast.LENGTH_SHORT).show();
                                getPassword.requestFocus();
                            }catch (Exception e){
                                Toast.makeText(getActivity(), "Login Failed", Toast.LENGTH_SHORT).show();
                                Log.e(TAG, TAG+ " "+ e);
                            }
                            progressDialog.hide();
                        }

                    }
                });
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, TAG+ " onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
        progressDialog.dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");
    }

}
