package com.example.xtrimitea.fragmenttransactions.ChatApp;

import com.example.xtrimitea.fragmenttransactions.SlidingTab_JsonRecyclerView.Singleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonInsertData {
    public static JSONObject vehicleType1, vehicleType2, vehicleType3, carBrand1, carBrand2, carBrand3,
            vehicleList1_1_1, vehicleList1_1_2, vehicleList1_2_1, vehicleList1_2_2, vehicleList1_3_1,vehicleList1_3_2,
            vehicleList2_1_1, vehicleList2_1_2, vehicleList2_2_1, vehicleList2_2_2, vehicleList2_3_1, vehicleList2_3_2,
            vehicleList3_1_1, vehicleList3_1_2, vehicleList3_2_1, vehicleList3_2_2, vehicleList3_3_1, vehicleList3_3_2
            ,allJson, allJson2;

    public static JSONObject mazdaObj1, mazdaObj2, mazdaObj3, mitsuObj1, mitsuObj2, mitsuObj3,
            toyotaLinkObj1, toyotaLinkObj2, toyotaLinkObj3;;

    public static JSONArray toyotaLink, toyotaLink2, toyotaLink3, mazdaLink, mazdaLink2, mazdaLink3, mitsuLink,
            mitsuLink2, mitsuLink3, allJson3, getVehicleType1, getVehicleType2, getVehicleType3;

    public static JSONObject getVehicleType11, getVehicleType22, getVehicleType33;

    public static JSONArray allVehicleTypes, allCarBrand, allVehicleList1_1, allVehicleList1_2, allVehicleList1_3,
            allVehicleList2_1, allVehicleList2_2, allVehicleList2_3, allVehicleList3_1, allVehicleList3_2, allVehicleList3_3;

    public static void main(String[] args) {
        try{
            vehicleType1 = new JSONObject().put("Vehicle Type", "Sedan");
            vehicleType2 = new JSONObject().put("Vehicle Type", "Sports Car");
            vehicleType3 = new JSONObject().put("Vehicle Type", "SUV");

            carBrand1 = new JSONObject().put("Car Brand", "Toyota");
            carBrand2 = new JSONObject().put("Car Brand", "Mazda");
            carBrand3 = new JSONObject().put("Car Brand", "Mitsubishi");

            vehicleList1_1_1 = new JSONObject().put("VehicleList", "Yaris");
            vehicleList1_1_2 = new JSONObject().put("VehicleList", "Corolla");

            vehicleList1_2_1 = new JSONObject().put("VehicleList", "Mazda 3");
            vehicleList1_2_2 = new JSONObject().put("VehicleList", "Mazda 6");

            vehicleList1_3_1 = new JSONObject().put("VehicleList", "Lancer EX");
            vehicleList1_3_2 = new JSONObject().put("VehicleList", "Mirage G4");

            vehicleList2_1_1 = new JSONObject().put("VehicleList", "GT86");
            vehicleList2_1_2 = new JSONObject().put("VehicleList", "S800");

            vehicleList2_2_1 = new JSONObject().put("VehicleList", "RX-7");
            vehicleList2_2_2 = new JSONObject().put("VehicleList", "RX-8");

            vehicleList2_3_1 = new JSONObject().put("VehicleList", "Lancer GSR");
            vehicleList2_3_2 = new JSONObject().put("VehicleList", "Lancer Evo");

            vehicleList3_1_1 = new JSONObject().put("VehicleList", "Highlander");
            vehicleList3_1_2 = new JSONObject().put("VehicleList", "RAV4");

            vehicleList3_2_1 = new JSONObject().put("VehicleList", "CX-3");
            vehicleList3_2_2 = new JSONObject().put("VehicleList", "CX-5");

            vehicleList3_3_1 = new JSONObject().put("VehicleList", "Outlander");
            vehicleList3_3_2 = new JSONObject().put("VehicleList", "Pajero");


            //Old JSON Insert Codes
//            allVehicleTypes = new JSONArray().put(vehicleType1).put(vehicleType2).put(vehicleType3);
//            allCarBrand = new JSONArray().put(carBrand1).put(carBrand2).put(carBrand3);
//
//            allVehicleList1_1 = new JSONArray().put(vehicleList1_1_1).put(vehicleList1_1_2);
//            allVehicleList1_2 = new JSONArray().put(vehicleList1_2_1).put(vehicleList1_2_2);
//            allVehicleList1_3 = new JSONArray().put(vehicleList1_3_1).put(vehicleList1_3_2);
//
//            allVehicleList2_1 = new JSONArray().put(vehicleList2_1_1).put(vehicleList2_1_2);
//            allVehicleList2_2 = new JSONArray().put(vehicleList2_2_1).put(vehicleList2_2_2);
//            allVehicleList2_3 = new JSONArray().put(vehicleList2_3_1).put(vehicleList2_3_2);
//
//            allVehicleList3_1 = new JSONArray().put(vehicleList3_1_1).put(vehicleList3_1_2);
//            allVehicleList3_2 = new JSONArray().put(vehicleList3_2_1).put(vehicleList3_2_2);
//            allVehicleList3_3 = new JSONArray().put(vehicleList3_3_1).put(vehicleList3_3_2);
//
//            allJson = new JSONObject().put("Vehicle Types", allVehicleTypes).put("Car Brands",allCarBrand)
//                    .put("Toyota Sedan", allVehicleList1_1).put("Mazda Sedan", allVehicleList1_2)
//                    .put("Mitsubishi Sedan", allVehicleList1_3).put("Toyota Sports Car", allVehicleList2_1)
//                    .put("Mazda Sports Car", allVehicleList2_2).put("Mitsubishi Sports Car", allVehicleList2_3)
//                    .put("Toyota SUV", allVehicleList3_1).put("Mazda SUV",allVehicleList3_2)
//                    .put("Mitsubishi SUV", allVehicleList3_3);

            String brand1 = Singleton.getInstance().setCarBrand1("Friend 1");
            String brand2 = Singleton.getInstance().setCarBrand2("Friend 2");
            String brand3 = Singleton.getInstance().setCarBrand3("Friend 3");

            toyotaLink = new JSONArray().put(vehicleList1_1_1).put(vehicleList1_1_2);
            toyotaLinkObj1 = new JSONObject().put("CarBrand", brand1).put(brand1+"Sedan", toyotaLink);
            mazdaLink = new JSONArray().put(vehicleList1_2_1).put(vehicleList1_2_2);
            mazdaObj1 = new JSONObject().put("CarBrand", brand2).put(brand2+"Sedan", mazdaLink);
            mitsuLink = new JSONArray().put(vehicleList1_3_1).put(vehicleList1_3_2);
            mitsuObj1 = new JSONObject().put("CarBrand", brand3).put(brand3+"Sedan", mitsuLink);

            getVehicleType1 = new JSONArray().put(toyotaLinkObj1).put(mazdaObj1).put(mitsuObj1);
            getVehicleType11 = new JSONObject().put("VehicleType","Sedan").put("CarBrands",getVehicleType1);

            toyotaLink2 = new JSONArray().put(vehicleList2_1_1).put(vehicleList2_1_2);
            toyotaLinkObj2 = new JSONObject().put("CarBrand", brand1).put(brand1+"SC", toyotaLink2);
            mazdaLink2 = new JSONArray().put(vehicleList2_2_1).put(vehicleList2_2_2);
            mazdaObj2 = new JSONObject().put("CarBrand", brand2).put(brand2+"SC", mazdaLink2);
            mitsuLink2 = new JSONArray().put(vehicleList2_3_1).put(vehicleList2_3_2);
            mitsuObj2 = new JSONObject().put("CarBrand", brand3).put(brand3+"SC", mitsuLink2);

            getVehicleType2 = new JSONArray().put(toyotaLinkObj2).put(mazdaObj2).put(mitsuObj2);
            getVehicleType22 = new JSONObject().put("VehicleType", "SportsCar").put("CarBrands",getVehicleType2);

            toyotaLink3 = new JSONArray().put(vehicleList3_1_1).put(vehicleList3_1_2);
            toyotaLinkObj3 = new JSONObject().put("CarBrand", brand1).put(brand1+"SUV", toyotaLink3);
            mazdaLink3 = new JSONArray().put(vehicleList3_2_1).put(vehicleList3_2_2);
            mazdaObj3 = new JSONObject().put("CarBrand", brand2).put(brand2+"SUV", mazdaLink3);
            mitsuLink3 = new JSONArray().put(vehicleList3_3_1).put(vehicleList3_3_2);
            mitsuObj3 = new JSONObject().put("CarBrand", brand3).put(brand3+"SUV", mitsuLink3);

            getVehicleType3 = new JSONArray().put(toyotaLinkObj3).put(mazdaObj3).put(mitsuObj3);
            getVehicleType33 = new JSONObject().put("VehicleType","SUV").put("CarBrands",getVehicleType3);

            allJson3 = new JSONArray().put(getVehicleType11).put(getVehicleType22).put(getVehicleType33);
            allJson2 = new JSONObject().put("VehicleTypes", allJson3);

//            System.out.println(allJson);
//            System.out.println(allJson2);


        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
