package com.example.xtrimitea.fragmenttransactions.SlidingTabFragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;



//How to create a simple tabs with fragment and viewpager
//1. Create Fragment class and layout (See FragmentA and B)
//2. Create PagerAdapter class
//3. Create layout for the main activity using toolbar, tablayout, and viewpager (See activity_main4.xml)

//2.1 Extend to FragmentStatePagerAdapter and implement methods and super
public class PagerAdapter extends FragmentStatePagerAdapter{
    int mNumOfTabs, mFragNum;

    //super
    public PagerAdapter(FragmentManager fm, int NumOfTabs, int FragNum) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.mFragNum = FragNum;
    }

    //methods
    @Override
    public Fragment getItem(int position) {

        //2.2 Create condition statement for each tabs
        if (mFragNum == 0) {
            if (position == 0) {
                SlideFrag1 fragment1 = new SlideFrag1();
                fragment1.mFragNum = 0;
                return fragment1;
            } else if (position == 1) {
                SlideFrag2 fragment2 = new SlideFrag2();
                fragment2.mFragNum = 0;
                return fragment2;
            } else if (position == 2) {
                SlideFrag3 fragment3 = new SlideFrag3();
                fragment3.mFragNum = 0;
                return fragment3;
            } else
                return null;
        }
        else if (mFragNum == 1){
            if (position == 0) {
                SlideFrag1_A fragment1 = new SlideFrag1_A();
                fragment1.mFragNum = 1;
                return fragment1;
            } else if (position == 1) {
                SlideFrag2_A fragment2 = new SlideFrag2_A();
                fragment2.mFragNum = 1;
                return fragment2;
            } else if (position == 2) {
                SlideFrag3_A fragment3 = new SlideFrag3_A();
                fragment3.mFragNum = 1;
                return fragment3;
            } else
                return null;
        }
        else
            return null;
    }

    @Override
    public int getCount() {
        //2.3 Return the number of tabs
        return mNumOfTabs;
    }
}
