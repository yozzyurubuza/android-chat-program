package com.example.xtrimitea.fragmenttransactions.FragmentTransactions;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xtrimitea.fragmenttransactions.R;

//2. Duplicate Fragment B and layout for Fragment B
public class FragmentC extends Fragment {

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("TAG","Fragment C onAttach");
    }

    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("TAG","Fragment C onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cc, container, false);
        Log.d("TAG","Fragment C onCreateView");
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("TAG","Fragment C onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TAG","Fragment C onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TAG","Fragment C onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("TAG","Fragment C onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TAG","Fragment C onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("TAG","Fragment C onDetach");
    }

}
