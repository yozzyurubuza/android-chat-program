package com.example.xtrimitea.fragmenttransactions.ChatApp;

public class ChatInfoRegFriends{
    private String email;

    public ChatInfoRegFriends(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
