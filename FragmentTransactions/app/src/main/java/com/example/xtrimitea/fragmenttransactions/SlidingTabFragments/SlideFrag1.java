package com.example.xtrimitea.fragmenttransactions.SlidingTabFragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;

import com.example.xtrimitea.fragmenttransactions.R;

public class SlideFrag1 extends Fragment {
    public static int mFragNum = 0;
    private TabLayout tabLayoutA;
    private ViewPager viewPagerA;
    private PagerAdapter pagerAdapter;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("TAG","SlideFrag1 onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("TAG","SlideFrag1 onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.fragment_a, container, false);

        //Second tabLayout
        tabLayoutA = (TabLayout) view.findViewById(R.id.tabLayoutA);
        tabLayoutA.addTab(tabLayoutA.newTab().setText("Tab 1"));
        tabLayoutA.addTab(tabLayoutA.newTab().setText("Tab 2"));
        tabLayoutA.addTab(tabLayoutA.newTab().setText("Tab 3"));
        tabLayoutA.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPagerA = (ViewPager) view.findViewById(R.id.viewPagerA);
        pagerAdapter = new PagerAdapter(getChildFragmentManager(), tabLayoutA.getTabCount(),1);
        viewPagerA.setAdapter(pagerAdapter);
        viewPagerA.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutA));
        tabLayoutA.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPagerA.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        Log.e("TAG", "ViewPager initialized");

        Log.d("TAG","SlideFrag1 onCreateView");
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("TAG","SlideFrag1 onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TAG","SlideFrag1 onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TAG","SlideFrag1 onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("TAG","SlideFrag1 onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TAG","SlideFrag1 onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("TAG","SlideFrag1 onDetach");
    }
}
