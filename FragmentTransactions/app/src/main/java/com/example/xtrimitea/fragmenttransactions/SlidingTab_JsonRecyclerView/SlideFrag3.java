package com.example.xtrimitea.fragmenttransactions.SlidingTab_JsonRecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xtrimitea.fragmenttransactions.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SlideFrag3 extends Fragment {

    //RecyclerView Codes
    private List<JsonData> items;
    private RecyclerView recyclerView;
    private ListAdapter listAdapter;
    private JSONObject jsonObject;
    private JSONArray jsonArray, jsonArray2;
    private JsonData jdata;

    private static final String TAG = SlideFrag3.class.getSimpleName();

    private FragmentManager manager;
    private FragmentTransaction transaction;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d("TAG","SlideFrag3 onAttach");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Log.d(TAG, "setUserVisibleHint in " + TAG);
            Singleton.getInstance().setMainFragPos(3);
            addFrag();
        }
        else{
            removeFrag();
        }
    }

    public void addFrag(){
        Act2_Frag2 frag2 = new Act2_Frag2();
        manager = Singleton.getInstance().getFragmentManager();
        transaction = manager.beginTransaction();
        transaction.add(R.id.frameLayout, frag2, "lowerTab3");
        transaction.commit();
        Log.d(TAG, "addFrag: in "+TAG);
    }

    public void removeFrag(){
        Act2_Frag2 frag2 = new Act2_Frag2();
        manager = Singleton.getInstance().getFragmentManager();
        transaction = manager.beginTransaction();
        if(frag2 != null) {
            transaction.remove(frag2);
            transaction.commit();
            Log.d(TAG, "Executing remove frag2 in " + TAG);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("TAG","SlideFrag3 onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        view = inflater.inflate(R.layout.stjrv_fragment_vlist, container, false);

        init(view);
        process();

        Log.d("TAG","SlideFrag3 onCreateView");
        return view;
    }

    private void init(View view) {
        jsonArray = Singleton.getInstance().getJsonArray();
        recyclerView = view.findViewById(R.id.recyclerViewCarBrand);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
    }

    private void process() {

        Singleton.getInstance().setJsonArrayBot(Singleton.getInstance().getJsonArray());

        try {
            jsonArray = Singleton.getInstance().getJsonArray();
            jsonObject = jsonArray.getJSONObject(0);
            jsonArray2 = jsonObject.getJSONArray("CarBrands");

            Singleton.getInstance().setItems(new ArrayList<>());
            items = Singleton.getInstance().getItems();

            for (int i = 0; i < jsonArray2.length(); i++) {
                jdata = Singleton.getInstance().setJdata(
                        new JsonData(jsonArray2.getJSONObject(i).getString("CarBrand")));
                items.add(Singleton.getInstance().getJdata());
            }

            listAdapter = new ListAdapter(getChildFragmentManager(), items);
            recyclerView.setAdapter(listAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("TAG","SlideFrag3 onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TAG","SlideFrag3 onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("TAG","SlideFrag3 onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("TAG","SlideFrag3 onDestroyView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("TAG","SlideFrag3 onDestroy");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d("TAG","SlideFrag3 onDetach");
    }

}
