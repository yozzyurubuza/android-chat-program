package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.xtrimitea.fragmenttransactions.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private FragmentManager context;
    private List<JsonData> items;
    private static final String TAG = ListAdapter.class.getSimpleName();
    private String[] friendImgURL;
    private Bitmap[] bitmaps;
    private Boolean[] isClickedForGC;
    private String[] friendUID, friendFullName;
    private String[] addFriendUID = new String[1000], addFriendFullName = new String[1000];
    private String[] gcName = new String[1000], gcImage = new String[1000];
    private List<String> addToGC = new ArrayList<String>();


    private OnItemClicked onClick;

    public interface OnItemClicked{
        void onItemClick(int position);
    }

    public ListAdapter(FragmentManager context, List<JsonData> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_recycler_format, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String[] fullNameArray = items.get(position).getTxt();
        holder.text1.setText(fullNameArray[position]);
        Log.e(TAG, "Holder Text1: "+fullNameArray[position]);

        //Opens up a chat window when a friend is clicked
        final int finalPosition = position;
        final ViewHolder finalholder = holder;
        holder.friendLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.onItemClick(finalPosition);

                //If ListAdapter will be used inside CreateGC Fragment
                if (Singleton.getInstance().getInCreateGC()){
                    storeDataForGC(finalPosition, finalholder);
                }

            }
        });


        if (!Singleton.getInstance().getDownloadImageFriends()) {
            friendImgURL = Singleton.getInstance().getNewFriendImgURL();
            holder.image1.execute(friendImgURL[position]);
            Log.e(TAG, "Downloading new image... : "+friendImgURL[position]);
        }
        Log.e(TAG,"ViewHolderPos: "+position);

    }

    public void storeDataForGC(int finalPosition, ViewHolder finalholder){
        isClickedForGC = Singleton.getInstance().getIsClickedForGC();
        friendUID = Singleton.getInstance().getFriendsUid();
        friendFullName = Singleton.getInstance().getNewFriendFullname();
        if(!isClickedForGC[finalPosition]) {
            finalholder.friendLayout.setBackgroundResource(R.drawable.chatbarborder_selected);
            addFriendUID[finalPosition] = friendUID[finalPosition];
            addFriendFullName[finalPosition] = friendFullName[finalPosition];
            isClickedForGC[finalPosition] = true;
            addToGC.add(addFriendUID[finalPosition]);
            Log.e(TAG, "addFriendUID: "+ Arrays.toString(addFriendUID));
            Log.e(TAG, "addFriendFullName: "+ Arrays.toString(addFriendFullName));
            Log.e(TAG, "isClickedForGC: "+ Arrays.toString(isClickedForGC));

            for (int i=0; i<addToGC.size();i++){
                if(addFriendUID[finalPosition].matches(addToGC.get(i))){
                    Log.e(TAG, "addToGC: "+ addToGC.get(i));
                    Log.e(TAG, "addToGC new size: "+addToGC.size());
                }
            }

            Singleton.getInstance().setAddToGC(addToGC);
        }

        else if(isClickedForGC[finalPosition]){
            finalholder.friendLayout.setBackgroundResource(R.drawable.chatbarborder);

            for (int i=0;i<addToGC.size();i++){
                if((addFriendUID[finalPosition].equals(addToGC.get(i)))){
                    Log.e(TAG, "Removing: "+addFriendUID[finalPosition]);
                    addToGC.remove(i);
                    Log.e(TAG, "addToGC new size: "+addToGC.size());
                }

            }

            addFriendUID[finalPosition] = "null";
            addFriendFullName[finalPosition] = "null";
            isClickedForGC[finalPosition]=false;
            Log.e(TAG, "addFriendUID: "+ Arrays.toString(addFriendUID));
            Log.e(TAG, "addFriendFullName: "+ Arrays.toString(addFriendFullName));
            Log.e(TAG, "isClickedForGC: "+ Arrays.toString(isClickedForGC));
            Singleton.getInstance().setAddToGC(addToGC);

        }
    }

    @Override
    public int getItemCount() {
        if(Singleton.getInstance().getListAdapterSize() != items.size()) {
            Singleton.getInstance().setListAdapterSize(items.size());
            Log.e(TAG, "Setting new ListAdapterSize... : " +items.size());
            Singleton.getInstance().setDownloadImageFriends(false);
        }
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private AsyncTask<String, Void, Bitmap> image1;
        private TextView text1;
        private ConstraintLayout friendLayout;
        private ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);

            image =  itemView.findViewById(R.id.receiverMsg);
            image1 = new Singleton.DownloadImageTask(image);
            text1 = itemView.findViewById(R.id.recycleTextChat);
            friendLayout = itemView.findViewById(R.id.friendLayout);

            //Retrieving data from the array of downloaded images
            if (Singleton.getInstance().getDownloadImageFriends()) {
                int pos = Singleton.getInstance().getListAdapterPos();

                pos++;
                Log.e(TAG, "ListAdapterPos: " + pos);
                try {
                    image.setImageBitmap(Singleton.getInstance().getImageListFriends(pos));
                }catch (Exception e){
                    e.printStackTrace();
                    image.setImageResource(R.drawable.noimage);
                }
                Log.e(TAG, "Using downloaded friend images...");

                if (Singleton.getInstance().getListAdapterSize() == pos){
                    pos = 0;
                }
                Singleton.getInstance().setListAdapterPos(pos);
            }

        }
    }

    public void setOnClick(OnItemClicked onClick){
        this.onClick=onClick;
    }


}
