package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.xtrimitea.fragmenttransactions.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GCListAdapter extends RecyclerView.Adapter<GCListAdapter.ViewHolder> {
    private FragmentManager context;
    private List<JsonData> items;
    private static final String TAG = GCListAdapter.class.getSimpleName();

    private String[] gcImage = new String[1000];



    private OnItemClicked onClick;

    public interface OnItemClicked{
        void onItemClick(int position);
    }

    public GCListAdapter(FragmentManager context, List<JsonData> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_recycler_format, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String[] fullNameArray = items.get(position).getTxt();
        holder.text1.setText(fullNameArray[position]);
        Log.e(TAG, "Holder Text1: "+fullNameArray[position]);

        //Opens up a chat window when a friend is clicked
        final int finalPosition = position;
        final ViewHolder finalholder = holder;
        holder.friendLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.onItemClick(finalPosition);

            }
        });


        if (!Singleton.getInstance().getDownloadGCImage()) {
            gcImage = Singleton.getInstance().getGcImage();
            holder.image1.execute(gcImage[position]);
            Log.e(TAG, "Downloading new gc image... : "+gcImage[position]);
        }

        Log.e(TAG,"ViewHolderPos: "+position);

    }

    @Override
    public int getItemCount() {
        if(Singleton.getInstance().getGcListAdapterSize() != items.size()) {
            Singleton.getInstance().setGcListAdapterSize(items.size());
            Log.e(TAG, "Setting new GCListAdapterSize... : " +items.size());
            Singleton.getInstance().setGcListAdapterPos(0);
            Singleton.getInstance().setGcPos(0);
            Singleton.getInstance().setDownloadGCImage(false);
        }

        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private AsyncTask<String, Void, Bitmap> image1;
        private TextView text1;
        private ConstraintLayout friendLayout;
        private ImageView image;
        private Bitmap[] bitmaps;

        public ViewHolder(View itemView) {
            super(itemView);

            image =  itemView.findViewById(R.id.receiverMsg);
            image1 = new Singleton.DownloadImageTask(image);
            text1 = itemView.findViewById(R.id.recycleTextChat);
            friendLayout = itemView.findViewById(R.id.friendLayout);
            bitmaps = Singleton.getInstance().getGcImageList();

            //Retrieving data from the array of downloaded images
            if (Singleton.getInstance().getDownloadGCImage()) {
                int pos = Singleton.getInstance().getGcListAdapterPos();

                Log.e(TAG, "GCListAdapterPos: " + pos);
                try {
                    image.setImageBitmap(bitmaps[pos]);
                }catch (Exception e){
                    e.printStackTrace();
                    image.setImageResource(R.drawable.noimage);
                }
                Log.e(TAG, "Using downloaded GC images...");

                pos++;
                if (Singleton.getInstance().getGcListAdapterSize() == pos){
                    pos = 0;
                    Log.e(TAG, "Reverting Pos because; GcListAdapterSize: "+Singleton.getInstance().getGcListAdapterSize());
                }
                Singleton.getInstance().setGcListAdapterPos(pos);
            }

        }
    }

    public void setOnClick(OnItemClicked onClick){
        this.onClick=onClick;
    }

}
