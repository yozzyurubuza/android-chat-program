package com.example.xtrimitea.fragmenttransactions.ChatApp;

class JsonData {

    private String[] txt = new String[1000];
    private String img;


    public JsonData(String[] txt, String img) {
        this.txt = txt;
        this.img = img;
    }

    public String[] getTxt() {
        return txt;
    }

    public void setTxt(String[] txt) {
        this.txt = txt;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

}
