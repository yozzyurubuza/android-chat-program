package com.example.xtrimitea.fragmenttransactions.ChatApp;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;

public class ChatFragHome extends Fragment {
    private static final String TAG = ChatFragHome.class.getSimpleName();
    private TextView lname, fname, cnum;

    private FragmentManager manager;
    private FragmentTransaction transaction;
    private ImageView displayPic;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ChatPagerAdapter pagerAdapter;


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_frag_home, container, false);
        Log.d(TAG, TAG+ " onCreateView");

        init(view);
        process(view);

        return view;
    }


    private void init(View view) {
        lname = view.findViewById(R.id.home_lname);
        fname = view.findViewById(R.id.home_fname);
        cnum = view.findViewById(R.id.home_cnum);
        displayPic = view.findViewById(R.id.displayPic);
        tabLayout = view.findViewById(R.id.chatTabLayout);
        viewPager = view.findViewById(R.id.chatViewPager);

        manager = getActivity().getSupportFragmentManager();


    }

    private void process(View view) {
        lname.setText(Singleton.getInstance().getLname());
        fname.setText(Singleton.getInstance().getFname());
        cnum.setText(Singleton.getInstance().getContactnum());

        //TabLayout Codes
        tabLayout.addTab(tabLayout.newTab().setText("Contact List"));
        tabLayout.addTab(tabLayout.newTab().setText("Group Chats"));
        tabLayout.addTab(tabLayout.newTab().setText("Settings"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        pagerAdapter = new ChatPagerAdapter(getChildFragmentManager(), tabLayout.getTabCount(),0);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        //addFrag();

        //Download Image URL
        if(Singleton.getInstance().getDownloadUserImage() == false) {
            new Singleton.DownloadImageTask((ImageView) view.findViewById(R.id.displayPic))
                    .execute(Singleton.getInstance().getUserimgurl());
            Log.e(TAG, "Downloading user image...");
        }

        else if(Singleton.getInstance().getDownloadUserImage() == true){
            displayPic.setImageBitmap(Singleton.getInstance().getImageUser());
            Log.e(TAG, "User Image: "+ Singleton.getInstance().getImageUser());
        }



    }

    public void addFrag(){
        ChatFragFlist flist = new ChatFragFlist();
        manager = Singleton.getInstance().getManager();
        transaction = manager.beginTransaction();
        transaction.add(R.id.flistLayout, flist, "Friends");
        transaction.commit();
        Log.d(TAG, "addFrag: in "+TAG);
    }

    public void removeFrag(){
        ChatFragFlist flist = new ChatFragFlist();
        manager = Singleton.getInstance().getManager();
        transaction = manager.beginTransaction();
        if(flist != null) {
            transaction.remove(flist);
            transaction.commit();
            Log.d(TAG, "Executing remove frag2 in " + TAG);
        }
    }




    public void exitDialog(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getActivity(), android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(getActivity());
        }
        //Dialog Box Yes or No
        builder.setTitle("Exit Application")
                .setMessage("Are you sure you want to Exit?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //Clear user data
                        Singleton.getInstance().clearData();

                        manager.popBackStack();
                        Log.e(TAG, "Exiting....");

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, TAG+ " onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");

    }


}
