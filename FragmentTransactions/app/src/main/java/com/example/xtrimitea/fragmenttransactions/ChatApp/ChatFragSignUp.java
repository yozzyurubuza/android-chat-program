package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.xtrimitea.fragmenttransactions.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ChatFragSignUp extends Fragment {

    private static final String TAG = ChatFragSignUp.class.getSimpleName();
    private FragmentManager manager;
    private FragmentTransaction transaction;

    private EditText getLname, getFname, getCnum, getEmail, getPass;
    private Button signUpBut;
    private ProgressDialog progressDialog;
    private View view;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference mDataBaseRoot, mDataBase2;
    private Boolean debugRegister = false;
    private int dataSnap;
    private String imageurl = "http://lequytong.com/Content/Images/no-image-02.png";

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, TAG+ " onCreateView");
        view = inflater.inflate(R.layout.chat_frag_signup, container, false);

        init(view);
        process(view);

        return view;
    }

    public void init(View view){
        progressDialog = new ProgressDialog(getActivity());
        getLname = view.findViewById(R.id.SU_lname);
        getFname = view.findViewById(R.id.SU_fname);
        getCnum = view.findViewById(R.id.SU_cnum);
        getEmail = view.findViewById(R.id.SU_email);
        getPass = view.findViewById(R.id.SU_pass);
        signUpBut = view.findViewById(R.id.SU_signUpBut);

        manager = getActivity().getSupportFragmentManager();
        firebaseAuth = FirebaseAuth.getInstance();
        mDataBaseRoot = FirebaseDatabase.getInstance().getReference().getRoot();
        mDataBase2 = FirebaseDatabase.getInstance().getReference();

    }

    public void process(View view){
        //When sign up button is clicked
        signUpBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Check info
                if (debugRegister == true) {
                    if (!getLname.getText().toString().isEmpty()) {
                        progressDialog.setMessage(Singleton.getInstance().getProgressReg());
                        progressDialog.show();
                        registerUser();
                    } else {
                        Toast.makeText(getActivity(), "Please do fill up the registration page", Toast.LENGTH_SHORT).show();
                    }
                }
                else if(debugRegister == false){
                    checkInfoInserted();
                }

            }
        });
    }


    private void checkInfoInserted() {
        if (getLname.getText().toString().isEmpty() || getLname.getText().toString().startsWith(" ")) {
            Toast.makeText(getActivity(), "Last Name field is empty", Toast.LENGTH_SHORT).show();
            getLname.requestFocus();
        }
        else if (getFname.getText().toString().isEmpty() || getFname.getText().toString().startsWith(" ")) {
            Toast.makeText(getActivity(), "First Name field is empty", Toast.LENGTH_SHORT).show();
            getFname.requestFocus();
        }
        else if (getCnum.getText().toString().isEmpty() || getCnum.getText().toString().startsWith(" ")) {
            Toast.makeText(getActivity(), "Contact Number field is empty", Toast.LENGTH_SHORT).show();
            getCnum.requestFocus();
        }
        else if (getEmail.getText().toString().isEmpty() || getEmail.getText().toString().startsWith(" ")) {
            Toast.makeText(getActivity(), "Email field is empty", Toast.LENGTH_SHORT).show();
            getEmail.requestFocus();
        }
        else if (getPass.getText().toString().isEmpty() || getPass.getText().toString().startsWith(" ")) {
            Toast.makeText(getActivity(), "Password field is empty", Toast.LENGTH_SHORT).show();
            getPass.requestFocus();
        }
        else {
            progressDialog.setMessage(Singleton.getInstance().getProgressReg());
            progressDialog.show();
            registerUser();
        }
    }

    public void registerUser(){

        //Add details into database (DEBUG MODE)
        if (debugRegister == true) {
            mDataBase2.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    Log.e(dataSnapshot.getKey(), dataSnapshot.getChildrenCount() + "");
                    dataSnap = (int) dataSnapshot.getChildrenCount()+1;
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            mDataBaseRoot.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    //Get email, fname, lname, contactnum
                    final ChatInfoRegister chatInfoRegister = new ChatInfoRegister(
                            getEmail.getText().toString(), getLname.getText().toString(), getFname.getText().toString(),
                            getCnum.getText().toString(), imageurl
                    );
                    // Register to database
                    mDataBaseRoot.child("users").child("uid_"+String.valueOf(dataSnap)).setValue(chatInfoRegister);
                    Toast.makeText(getActivity(), "Data inserted", Toast.LENGTH_SHORT).show();
                    //Only use this commands when debugRegister is false
                    progressDialog.hide();
                    Toast.makeText(getActivity(), "Registration is succesful", Toast.LENGTH_SHORT).show();
                    //manager.popBackStack();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }


        //Register Email and Pass separately (2 different DB)
        if (debugRegister == false) {
            //Get number of users first
            mDataBase2.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    Log.e(dataSnapshot.getKey(), dataSnapshot.getChildrenCount() + "");
                    dataSnap = (int) dataSnapshot.getChildrenCount()+1;
                    Singleton.getInstance().setNumOfRegUsers(dataSnap);
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            //Register user details into Firebase Database
            firebaseAuth.createUserWithEmailAndPassword(getEmail.getText().toString(), getPass.getText().toString())
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                //No errors are found upon inputting the required user info

                                //Get email, fname, lname, contactnum
                                final ChatInfoRegister chatInfoRegister = new ChatInfoRegister(
                                        getEmail.getText().toString(), getLname.getText().toString(), getFname.getText().toString(),
                                        getCnum.getText().toString(), imageurl
                                );

                                final ChatInfoRegFriends regFriends = new ChatInfoRegFriends(
                                        getEmail.getText().toString()
                                );

                                //Insert to DB
                                mDataBaseRoot.child("users")
                                        .child("uid_" + String.valueOf(Singleton.getInstance().getNumOfRegUsers()))
                                        .setValue(chatInfoRegister);

                                mDataBaseRoot.child("userfriends")
                                        .child("uid_" + String.valueOf(Singleton.getInstance().getNumOfRegUsers()))
                                        .setValue(regFriends);

                                progressDialog.hide();
                                Toast.makeText(getActivity(), "Registration is succesful", Toast.LENGTH_SHORT).show();
                                manager.popBackStack();

                            } else if (!task.isSuccessful()) {
                                //Check error based on the standards of Firebase
                                try {
                                    throw task.getException();
                                } catch (FirebaseAuthWeakPasswordException e) {
                                    Toast.makeText(getActivity(), "Weak Password", Toast.LENGTH_SHORT).show();
                                    getPass.requestFocus();
                                    progressDialog.hide();
                                } catch (FirebaseAuthInvalidCredentialsException e) {
                                    Toast.makeText(getActivity(), "Invalid Email", Toast.LENGTH_SHORT).show();
                                    getEmail.requestFocus();
                                    progressDialog.hide();
                                } catch (FirebaseAuthUserCollisionException e) {
                                    Toast.makeText(getActivity(), "Registered email found", Toast.LENGTH_SHORT).show();
                                    getEmail.requestFocus();
                                    progressDialog.hide();
                                } catch (Exception e) {
                                    Log.e(TAG, e.getMessage());
                                    progressDialog.hide();
                                }

                            } else {
                                Toast.makeText(getActivity(), "Registration not successful", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });



        }

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, TAG+ " onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");
    }


}
