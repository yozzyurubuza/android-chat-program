package com.example.xtrimitea.fragmenttransactions.ChatApp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xtrimitea.fragmenttransactions.R;

import java.util.Arrays;
import java.util.List;

public class ChatFragCreateGCFlist extends Fragment {
    private static final String TAG = ChatFragCreateGCFlist.class.getSimpleName();

    private RecyclerView recyclerGC;
    private List<JsonData> items;
    private ListAdapter listAdapter;
    private Boolean[] isClickedForGC;


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            Log.d(TAG, TAG+ " setUserVisibleHint");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, TAG+ " onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, TAG+ " onCreate");

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_recycler_flist, container, false);
        Log.d(TAG, TAG+ " onCreateView");

        init(view);
        process(view);

        return view;
    }

    private void init(View view){
        Singleton.getInstance().setInCreateGC(true);
        recyclerGC = view.findViewById(R.id.recyclerFriends);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerGC.setLayoutManager(layoutManager);

        items = Singleton.getInstance().getStoredList();

    }

    private void process(View view){
        //Set false to the list of clicked friends to be added for GC
        isClickedForGC = Singleton.getInstance().getIsClickedForGC();
        for(int i=0; i<isClickedForGC.length; i++){
            isClickedForGC[i] = false;
        }
        Singleton.getInstance().setIsClickedForGC(isClickedForGC);


        //Use the same listAdapter from ChatFragHome list
        listAdapter = new ListAdapter(getActivity().getSupportFragmentManager(), items);
        listAdapter.setOnClick(new ListAdapter.OnItemClicked() {
            @Override
            public void onItemClick(int position) {
                Log.e(TAG, "Friend #"+position+" is selected!");


            }
        });
        recyclerGC.setAdapter(listAdapter);


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, TAG+ " onActivityCreated");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, TAG+ " onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Singleton.getInstance().setInCreateGC(false);
        Log.d(TAG, TAG+ " onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, TAG+ " onDestroy");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, TAG+ " onDestroyView");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG, TAG+ " onDetach");

    }


}
